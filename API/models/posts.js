const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
    subject: { type: String, required: true },
    text: { type: String, required: true },
    // tags : {type : Array, default : []},
    dateOfPublish: { type: Date, default: Date.now() },
    author: { type: String, required: true },
    comments: [Schema.Types.ObjectId]
});

module.exports = mongoose.model('posts', mySchema);