const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
    text : String,
    author : String,
    post : Schema.Types.ObjectId,
    // isReply : Boolean,
    // replyTo : Schema.Types.ObjectId,
    date : {type : Date, default : Date.now()},
    likes : Array
})

module.exports = mongoose.model('comments', mySchema);
