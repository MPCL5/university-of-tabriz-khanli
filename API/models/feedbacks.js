const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var mySchema = new Schema({
    name : String,
    subject : String,
    text : String,
    date : {type : Date, default : Date.now()}
})

module.exports = mongoose.model('feedbacks', mySchema);