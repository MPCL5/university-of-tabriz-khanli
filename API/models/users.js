const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var mySchema = new Schema({
    fullName: String,
    firstName: String,
    lastName: String,
    username: String,
    friends: Array,
    email: String,
    password: String,
    bio: String,
    profileImage: String,
    job: String,
    field: String,
    educations: String,
    yearOfJoin: String,
    phone: String,
    telegram: String,
    instagram: String,
    twitter: String,
});

module.exports = mongoose.model('users', mySchema);