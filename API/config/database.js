module.exports = function(mongoose) {
    const mongoDB = 'mongodb://localhost/project'

    mongoose.connect(mongoDB,{useNewUrlParser : true})
    const db = mongoose.connection

    // monitor databse connection :|.
    db.on('error', err => console.log(`Mongoose: connection error: \n ${err}`))
    db.on('connected',() => console.log('Mongoose: connected successfully.'))

    // If the Node process ends, close the Mongoose connection. so intersted
    process.on('SIGINT', () => {
        db.close(() => {
            console.log('\n default connection closed successfully.')
            process.exit(0)
        })
    })
}