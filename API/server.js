const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const path = require('path')

const app = express()

const Port = process.env.PORT || 80

// set up body parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// configure mongoose
require('./config/database')(mongoose)

const mainRouter = require('./Routes/main')
const authRouter = require('./Routes/auth')
const panelRouter = require('./Routes/panel')
const postRouter = require('./Routes/post')

// from stack overflow: I forget to add link
app.use((req, res, next) => {
    const origin = req.get('origin');

    // TODO Add origin validation
    res.header('Access-Control-Allow-Origin', origin)
    res.header('Access-Control-Allow-Credentials', true)
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma')

    // intercept OPTIONS method
    if (req.method === 'OPTIONS') {
        res.sendStatus(204)
    } else {
        next()
    }
});

app.use('/static', express.static(path.join(__dirname, 'static')))

app.use('/', mainRouter)

app.use('/auth', authRouter)

app.use('/panel', panelRouter)

app.use('/post', postRouter)

app.listen(Port, () => console.log('Server is listening on port: ' + Port))