const Users = require('../../models/users');

module.exports = function(req, res) {
    let doc = {}

    // if request have file set it to database. bad english it's 2:02 AM :(
    if (req.file)
        doc = { profileImage: req.file.filename }

    doc = {...doc, ...req.body }

    console.log(req.body);
    Users.updateOne({ username: req.username }, doc)
        .then(result => {
            if (result.ok)
                return res.send({ success: true });

            return res.send({ success: false });
        });
}