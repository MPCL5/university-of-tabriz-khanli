// based on article from: https://codeburst.io/image-uploading-using-react-and-node-to-get-the-images-up-c46ec11a7129
const multer = require('multer')
const path = '/home/mpcl5/Desktop/Node/project/university-of-tabriz-khanli/API/static/images/profiles/'

// where image shoud save.
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + req.username + '-' + Date.now())
    }
})

// A filter for incomeing files.
const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

// configure the multer.
const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 5 }, // limit image size to 5mb
    fileFilter: fileFilter
})

module.exports = upload.single('avatar')