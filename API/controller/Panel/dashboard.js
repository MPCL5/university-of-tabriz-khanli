const Users = require('../../models/users');

module.exports = function(req, res) {
    if (!req.userId) return res.status(500).send({ error: 'something went wrong.' })

    Users.findById(req.userId, 'name email')
        .then(user => {
            res.status(200).send(user)
        })
        .catch(() => res.status(500).send({ error: 'Ther was a problem during querying database.' }));
}