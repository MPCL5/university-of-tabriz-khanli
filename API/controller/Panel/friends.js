const Users = require('../../models/users')

exports.addFriend = function(req, res) {
    const user = req.username;
    const friendUsername = req.body.friend;

    // avoiding bad requests.
    if (!user || !friendUsername)
        return res.send({ success: false, error: "Bad API request." })

    Users.update({ username: user }, { $push: { "friends": friendUsername } })
        .then(result => {
            if (result.nModified)
                res.send({ success: true });
        })
        .catch(error => res.send({ success: false, error: "database error." }));
}

exports.getFriends = function(req, res) {
    const user = req.body.username;
    const rawLimit = req.body.limit || '20'; // limit search results
    const limit = Number(rawLimit)

    // avoiding bad requests.
    if (!user)
        return res.send({ success: false, error: "Bad API request." });

    // Step1. get the friends usernames.
    Users.findOne({ username: user }, 'friends')
        .then(result => {
            const friends = result.friends

            console.log(friends, result);

            if (!friends)
                return res.send({ success: false, error: "Bad API request " });

            // Step2. query the friends.
            Users.find({ username: { $in: friends } }, '-_id fullName field yearOfJoin username profileImage', { limit: limit })
                .then(finalResults => {
                    res.send({ success: true, results: finalResults })
                });
        })
        .catch(error => res.send({ success: false, error: "database error." }))
}