const Users = require('../../models/users');

module.exports = function(req, res) {
    const user = req.username

    // get the user friends.
    Users.findOne({ username: user }, 'friends')
        .then(result => {
            const userFriends = result.friends

            // TODO: need to add sorting.

            // suggest users which is not the user or one of the friends.
            Users.find({ username: { $nin: [...userFriends, user] } }, 'username fullName bio profileImage fullName', { limit: 3 })
                .then(results => {
                    res.send(results)
                })
        })
        .catch((error) => res.send({ success: false, error: 'Something went wrong!\n' }))
}