const Users = require('../../models/users')

module.exports = function(req, res) {
    console.log(req);

    if (!req.file)
        return res.send({ success: false })
    else {
        Users.updateOne({ username: req.username }, { profileImage: req.file.filename })
            .then(result => {

                if (result.ok) {
                    console.log(req.file.filename);
                    return res.send({ success: true, fileName: req.file.filename })
                } else
                    return res.send({ success: false })
            })
            .catch(() => res.send({ success: false }))
    }
}