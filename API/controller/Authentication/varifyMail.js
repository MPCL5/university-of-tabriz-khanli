module.exports = function(email) {
    if(!email.includes('@'))
        return false

    // fixed problem when email have extera dot.
    email = email.split('@')
    email[0] = email[0].replace('.', '')
    email = email.join('@')

    // from: https://stackoverflow.com/a/46181
    const reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(email);
}