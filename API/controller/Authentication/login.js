const bcrypt = require('bcryptjs')
const verifyMail = require('./varifyMail')

// JWT imports
const jwt = require('jsonwebtoken')
const secret = require('../../config/jwt').secret

const Users = require('../../models/users')

module.exports = function(req, res) {
    if (!req.body.username || !req.body.password)
        return res.send({ auth: false, error: 'لطفا ایمیل یا پسورد خود را وارد کنید' })

    const username = req.body.username.toLowerCase()
    const query = {}

    if (verifyMail(username))
        Object.assign(query, { email: username })
    else
        Object.assign(query, { username: username })

    // find the user
    Users.findOne(query)
        .then(user => {
            if (!user)
                return res.send({ error: 'کاربر مورد نظر یافت نشد' })

            // check password.
            const passwordIsValid = bcrypt.compareSync(req.body.password, user.password)
            if (!passwordIsValid)
                return res.send({ auth: false, token: null })

            // assigen token
            const token = jwt.sign({ id: user._id, username: user.username, fullName: user.fullName }, secret, { expiresIn: 28800 })

            res.send({ auth: true, token: token, profileImage: user.profileImage })
        })
        .catch(err => res.send({ error: 'Server side error.' }))
}