const bcrypt = require('bcryptjs');
const veifyMail = require('./varifyMail')

const Users = require('../../models/users');

module.exports = function(req, res) {
    try {
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;

        console.log(req.body);


        if (!username || !email || !password)
            return res.send({ success: false, error: 'لطفا تمامی اطلاعات خواسته شده را وارد کنید' });

        if (!veifyMail(email))
            return res.send({ success: false, error: 'آدرس پست الکترونیکی شما معتبر نمی باشد' });

        username = username.toLowerCase()
        email = email.toLowerCase()

        // check the email or username for avoiding duplicates.
        Users.find({ $or: [{ username: username }, { email: email }] }, 'email username')
            .then(result => {
                if (result.length > 0) {
                    if (username == result[0].username)
                        return res.send({ success: false, error: 'نام کاربری قبلا انتخاب شده است' })
                    else
                        return res.send({ success: false, error: 'پست الکترونیکی قبلا انتخاب شده است' })
                }

                // hash the password with the random salt.
                password = bcrypt.hashSync(password, 8)

                // create user document.
                Users.create({ username: username, email: email, password: password })
                    .then(() => res.send({ success: true }))
                    .catch(error => res.send({ success: false, error: 'An error occured during saving data: ' + error }))
            })
            .catch(error => res.send({ success: false, error: 'An error occured during querying: ' + error }))

    } catch (error) {
        res.send({ error: 'baad format' })
    }
}