const comment = require('../models/comments')

exports.new = function(req, res) {
    comment.create({
            text: req.body.text,
            author: req.user.username,
            post: req.body.postID,
            date: Date.now()
        })
        .then(() => res.send({ success: true }))
        .catch(() => res.send({ success: false, error: 'There was a problrm in creating new comment.' }))
}

exports.like = function(req, res) {
    comment.findOneAndUpdate({ _id: req.body.commentID }, {
            $push: { likes: req.user.username }
        })
        .then(() => res.send({ success: true }))
        .catch(() => res.send({ success: false, error: 'There was a problrm in submiting your like.' }))
}

exports.edite = function(req, res) {

}

exports.reply = function(req, res) {

}