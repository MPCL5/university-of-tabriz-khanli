const jwt = require('jsonwebtoken');
const secret = require('../config/jwt').secret

exports.token = function(req, res, next) {
    if (!req.headers['authorization'])
        return res.send({ auth: false, message: 'No Authorization header provided.' })

    const authRequest = req.headers['authorization'].split(' ');

    if (authRequest[0] != 'Bearer' || !authRequest[1])
        return res.send({ auth: false, message: 'No token provided.' });

    const token = authRequest[1];

    jwt.verify(token, secret, (err, decoded) => {
        if (err)
            return res.send({ auth: false, message: `Failed to authenticate token with error ${err.message} .` });

        if (decoded.exp < Date.now() / 1000)
            return res.send({ auth: false, message: 'The token has been expired' });

        req.userId = decoded.id;
        req.username = decoded.username;

        next();
    });
}