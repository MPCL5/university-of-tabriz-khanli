const Posts = require('../models/posts')

exports.getPosts = function(req, res) {
    let limit = Number(req.body.limit)
    if (!limit || limit > 20)
        limit = 10

    // TODO: we need to edite this accourding to friends posts
    Posts.find({}, null, { limit: limit })
        .then((results => {
            res.send(results)
        }))
        .catch(e => {
            res.send({ error: 'Database error' })
        })
}

exports.sendPost = function(req, res) {
    Posts.create({
            subject: req.body.subject,
            text: req.body.text,
            dateOfPublish: new Date(),
            author: req.username,
            comments: []
        })
        .then(() => {
            res.send({ success: true })
        })
        .catch(() => {
            res.send({ success: false, error: 'there was a problrm in creating new post' })
        })
}

exports.editePost = function(req, res) {
    try {
        const changes = JSON.parse(req.body.changes)

        console.log(changes)

        Posts.replaceOne({ _id: req.body.id, author: req.uusername }, { author: req.username, ...changes })
            .then(result => {
                if (result.n != 1)
                    return res.send({ success: false, error: 'Post is not match or you can not edit this post.' })

                res.send({ success: true })
            })
            .catch(() => res.send({ success: false, error: 'Data base error' }))
    } catch (error) {
        return res.send({ success: false, error: 'Json Parsing error' })
    }
}