const Users = require('../../models/users');

exports.profile = function(req, res) {
    const username = req.body.username

    // avoiding fake requests.
    if (!username)
        return res.status(403).send({ error: 'No Username provided.' });

    Users.findOne({ username: username.toLowerCase() })
        .then(result => {
            if (!result)
                return res.status(500).send({ error: 'User not found.' })

            res.status(200).send(result);
        })
        .catch(() => res.status(500).send({ error: 'Ther was a problem during querying database.' }));
}