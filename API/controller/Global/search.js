const Users = require('../../models/users')

module.exports = function(req, res) {
    // preparing dataase query
    const query = req.body.query

    if (query.length <= 3)
        return res.send({ error: 'query length must be more than 3.' })

    // console.log(query);

    // from: https://stackoverflow.com/a/10616781
    Users.find({ fullName: { $regex: `.*${query}.*` } })
        .then(results => {
            res.send(results)
        })
}