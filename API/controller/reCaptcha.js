const request = require('request');

const secretKey = require('../config/reCaptcha');

module.exports = function(req, res, next) {
    // console.log(req.body)
    if(req.body.captcha === undefined || req.body.captcha === '' || req.body.captcha === null){
      return res.json({success : false, error : "Please select captcha"});
    }  
    // Verify URL
    const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;
    
    // Make Request To VerifyURL
    request(verifyUrl, (err, response, body) => {
        if(err) return res.send({success : false, error : err})

        body = JSON.parse(body);
        // console.log(body);
  
        // If Not Successful
        if(body.success !== undefined && !body.success) {
            // TODO: we need to add some security codes there.
            return res.json({success : false, error : "Failed captcha verification"});
        }
  
        //If Successful
        next()
        // res.send(body)
    });
}