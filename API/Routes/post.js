const Router = require('express').Router();
const posts = require('../controller/post');
const comment = require('../controller/comment');
const verifyToken = require('../controller/verify').token;

Router.post('/', posts.getPosts)

Router.post('/new', verifyToken, posts.sendPost)

Router.post('/edite', verifyToken, posts.editePost)

Router.post('/new-comment', verifyToken, comment.new)

Router.post('/reply-comment', verifyToken, comment.reply)

Router.post('/like-comment', verifyToken, comment.like)

Router.post('/edite-comment', verifyToken, comment.edite)

module.exports = Router;