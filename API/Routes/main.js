const express = require('express')
const path = require('path')
const Router = express.Router();

const globals = require('../controller/Global/profile');
const search = require('../controller/Global/search')

Router.use('/', express.static(path.join(__dirname, '../Client')));

Router.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../Client', 'index.html'));
});

Router.post('/profile', globals.profile);

Router.post('/search', search);

module.exports = Router