const Router = require('express').Router()

const login = require('../controller/Authentication/login')
const verifyIdentity = require('../controller/Authentication/registerStepOne')
const register = require('../controller/Authentication/registerStepTwo')

Router.post('/login', login)

Router.post('/verifyIdentity', verifyIdentity)

Router.post('/register', register)

module.exports = Router