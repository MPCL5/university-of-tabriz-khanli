const Router = require('express').Router();

// controlles import
const dashboard = require('../controller/Panel/dashboard');
const verify = require('../controller/verify');
const imageUploadHandler = require('../controller/Panel/imageUploadHandler');
const edite = require('../controller/Panel/edite');
const editeProfileImage = require('../controller/Panel/editeProfileImage');
const suggest = require('../controller/Panel/suggest')
const friends = require('../controller/Panel/friends')

Router.post('/dashboard', verify.token, dashboard);

Router.post('/editeProfileImage', verify.token, imageUploadHandler, editeProfileImage);

Router.post('/editeProfile', verify.token, edite);

Router.post('/suggest', verify.token, suggest);

Router.post('/addFriend', verify.token, friends.addFriend);

Router.post('/getFriends', friends.getFriends)

module.exports = Router;