import Request, { API_Server } from './Request'
import user from '../Assets/Images/user.png' // default user image.

export default function(username) {
    return Request('profile', null, {
        method: 'POST',
        body: JSON.stringify({
            username: username
        })
    })
}

export function getProfileImage(imageURL) {
    if (imageURL)
        return `${API_Server}/static/images/profiles/${imageURL}`
    else
        return user
}