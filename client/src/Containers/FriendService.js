import request from './Request'
import AuthService from './AuthService'

function getFriends(username, limit = 20) {
    const URL = 'panel/getFriends'
    const config = {
        method: 'POST',
        body: JSON.stringify({
            limit: limit,
            username: username
        })
    }

    return request(URL, AuthService.getToken(), config)
}

function addFriend(friend) {
    const URL = 'panel/addFriend'
    const config = {
        method: 'POST',
        body: JSON.stringify({
            friend: friend
        })
    }
    const token = AuthService.getToken()

    return request(URL, token, config)
}

export { getFriends, addFriend }