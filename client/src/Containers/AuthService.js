import decode from 'jwt-decode'

import request from './Request'

class AuthService {
    constructor() {
        // this.domain = domain || 'auth'
        // set api server in request.js
    }

    // login user using email or username and password.
    // you can send use email in username arg.
    lgoin(username, password) {
        return request('auth/login', null, {
            method: 'POST',
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
    }

    // remove the token from user storage actually this is not a real logout :)  .
    logOut() {
        localStorage.removeItem('id_token')
        localStorage.removeItem('profileImage')
    }

    // save token to user storage.
    setToken(token) {
        localStorage.setItem('id_token', token)
    }

    // retrive token from user storage.
    static getToken() {
        return localStorage.getItem('id_token')
    }

    getToken() {
        return localStorage.getItem('id_token')
    }

    // check the user Auth status.
    isLoggedIn() {
        const token = this.getToken()

        if (token && !this.isExpired(token)) {
            return true
        } else {
            this.logOut()
            return false
        }
    }

    // Checking if token is expired.
    isExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            } else
                return false;
        } catch (err) {
            return false;
        }
    }

    getUserTokenData() {
        const token = this.getToken()

        if (!token)
            return null

        return decode(token)
    }
}

export default AuthService