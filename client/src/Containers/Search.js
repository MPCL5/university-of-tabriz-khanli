import request from './Request'

export default function(query) {
    return request('search', null, {
        method: 'POST',
            body: JSON.stringify({
                query : query
            })
    })
}