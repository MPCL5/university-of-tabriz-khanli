import axios from 'axios';

import request, { API_Server } from './Request'
import AuthService from './AuthService'

export function registerStepOne(identityCode, yearOfJoin) {
    const URL = 'auth/verifyIdentity'
    const config = {
        method: 'POST',
        body: JSON.stringify({
            identityCode: identityCode,
            yearOfJoin: yearOfJoin
        })
    }

    return request(URL, null, config)
}

export function registerStepTwo(username, email, password) {
    const URL = 'auth/register'
    const config = {
        method: 'POST',
        body: JSON.stringify({
            username: username,
            email: email,
            password: password
        })
    }

    return request(URL, null, config)
}

export function editeProfile(data, token) {
    const URL = 'panel/editeProfile'
    const config = {
        method: 'POST',
        body: JSON.stringify({
            ...data
        })
    }

    return request(URL, token, config)
}

export function editeProfileImage(imageData, token) {
    const URL = `${API_Server}/panel/editeProfileImage`
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + token
        }
    }

    return axios.post(URL, imageData, config)
}

export function getSuggestions() {
    const URL = 'panel/suggest'
    const config = {
        method: 'POST',
    }

    return request(URL, AuthService.getToken(), config)
}