export const API_Server = 'http://localhost';
// export const API_Server = 'http://88.99.119.204';

// A customized fetch function.
export default function(url, token, options) {
    // performs api calls sending the required authentication headers
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
    if (token) {
        headers['Authorization'] = 'Bearer ' + token
    }

    return fetch(`${API_Server}/${url}`, {
            headers,
            ...options
        })
        .then(response => response.json())
}