import React, { Component } from 'react'

import AuthService from './AuthService'

export default function(AuthComponent, flag) {
    // the flag show your need of auth
    return class handler extends Component {
        constructor(props) {
            super(props)

            this.Auth = new AuthService()
            this.loggedIn = this.Auth.isLoggedIn()
            console.log(this.loggedIn);
        }

        render() {
            if ((this.loggedIn && flag) || (!this.loggedIn && !flag))
                return <AuthComponent {...this.props }
            authProp = { this.Auth }
            />
            else
                return null
        }
    }
}