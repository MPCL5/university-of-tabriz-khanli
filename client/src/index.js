import React from 'react'
import ReactDOM from 'react-dom'
import { Link, Route, Switch, BrowserRouter as Router } from 'react-router-dom';

//Component Region
import Home from './Components/HomePage/Home';
import SignIn from './Components/SignIn/SignIn';
import SignUp from './Components/SignUp/SignUp';
import Search from './Components/Search/Search'
import Profile from './Components/Profile/Profile'
import UserArea from './Components/User Area/UserArea';
import UserEdit from './Components/User Area/UserEdit/UserEdit';
//Component Region End

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact = {true}  path = '/' component = {Home} ></Route>
            <Route path = '/signin' component = {SignIn}></Route> 
            <Route path = '/signup' component = {SignUp}></Route> 
            <Route path = '/search/:query' component = {Search}></Route>
            <Route path = '/search/' component = {Search}></Route>
            <Route path = '/users/:userid' component = {Profile}></Route>
            <Route path = '/users/' component = {Profile}></Route>
            <Route path = '/uarea' exact = {true} component = {UserArea} ></Route>
            <Route path = '/uarea/edit' component = {UserEdit} ></Route>
        </Switch>
    </Router>
    ,document.getElementById('root')
)