import React, {Component} from 'react';

//Style Region
import './Menu.css'
//Sytle region End

class MenuButton extends Component {
    constructor(props){
        super(props)
    }
    render () {
        return (
            <div>
                <button className = 'menubutton' onClick={window.open(this.props.to)} >
                    {this.props.text}
                </button>
            </div>
        )
    }
}

export default MenuButton