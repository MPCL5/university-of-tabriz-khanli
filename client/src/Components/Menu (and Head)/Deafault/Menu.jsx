import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Style Region
import './Menu.css';
//Style Region End

//Component Region
import MenuButton from './MenuButton';
import MobMenuButton from './MobMenuButton';

class Menu extends Component {
    constructor() {
        super()

        this.state = {
            open: false,
            navWidth: 0,
            zindex: -1,
            isSearch: window.location.href.includes('search')
        }

        this.SideNavRef = React.createRef()
    }

    slide() {
        if (this.state.open) {
            this.setState({
                open: false,
                navWidth: '0%',
                zindex: -1
            })
        } else {
            this.setState({
                open: true,
                navWidth: '100%',
                zindex: +1
            })
        }
    }

    reDirect(route = '') {
        window.location.href = window.location.origin + '/' +route
    }

    handleSignIn() {
        this.reDirect('signin')
    }

    handleSignUp() {
        this.reDirect('signup')
    }

    handleSerach() {
        this.reDirect('search')
    }

    handleAboutUs() {
        this.reDirect();
    }

    render() {
        return (
            <div>
                <MediaQuery minWidth={766} >
                    <div className='head' >
                        <p id='bigtitle' >
                            جامعه مجازی فارغ التحصیلان دانشگاه تبریز
                        </p>
                        <p id='smalltitle' >
                            فعلا داریم رو این قسمت کار میکنیم...
                        </p>
                    </div>
                    <div className='smallmenu' >
                        {!this.state.isSearch && <div className='searchbox' >
                            <button className='searchbutton' onClick={() => this.handleSerach()}>
                                جست و جوی پیشرفته
                            </button>
                            {/* <input className='searchinput' type="text" placeholder='چیزی وارد کنید...' /> */}
                        </div>}
                        <div className='menubox' >
                            <a className='amenu' onClick={() => this.handleAboutUs()} ><button className='menubutton' >درباره ما</button></a>
                            <a className='amenu' onClick={() => this.handleSignUp() } ><button className='menubutton' >ثبت نام</button></a>
                            <a className='amenu' onClick={() => this.handleSignIn() } ><button className='menubutton' >ورود</button></a>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobhead' >
                        <p id='bigtitle' >
                            جامعه مجازی فارغ التحصیلان دانشگاه تبریز
                        </p>
                        <p id='smalltitle' >
                            فعلا داریم رو این قسمت کار میکنیم...
                        </p>
                    </div>
                    <div className='mobsmallmenu' >
                        <div className='mobsearchbox'>
                            {(() => {
                                if (!this.state.isSearch)
                                    return (
                                        <>
                                            <button className='mobsearchbutton' onClick={() => this.handleSerach()} >
                                                جست و جوی پیشرفته
                                            </button>
                                        </>
                                    )
                            })()}
                        </div>
                        <div className='mobslidebox' >
                            <span className='mobmenuspan' onClick={this.slide.bind(this)} >
                                &#9776;
                            </span>
                        </div>
                    </div>
                    <div className='mobsidenav' style={{ width: this.state.navWidth }} >
                        <button className='mobmenubutton' onClick={() => this.handleAboutUs()} >ورود</button>
                        <button className='mobmenubutton' onClick={() => this.handleSignUp()} >ثبت نام</button>
                        <button className='mobmenubutton' onClick={() => this.handleSignIn()} >درباره ما</button>
                    </div>
                </MediaQuery>
            </div >
        )
    }
}

export default Menu