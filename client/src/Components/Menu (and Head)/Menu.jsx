import React, { Component } from 'react';

//Component Region
import UAMenu from './UserArea/Menu'
import DefaultMenu from './Deafault/Menu'
//Component Region End

import AuthService from '../../Containers/AuthService'

class Menu extends Component {
    constructor(props) {
        super(props)
        this.AuthService = new AuthService()
        this.userData = this.AuthService.getUserTokenData()        
    }

    render() {
        return (
            <>
                {this.AuthService.isLoggedIn() ? <UAMenu {...this.props}/> : <DefaultMenu />}
            </>
        )
    }
}

export default Menu