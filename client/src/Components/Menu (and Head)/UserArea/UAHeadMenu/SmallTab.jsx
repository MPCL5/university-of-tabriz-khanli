import React, { Component } from 'react';

import AuthService from '../../../../Containers/AuthService'
import { getProfileImage } from '../../../../Containers/Profile'

import './UAHeadMenu.css';

class SmallTab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEdit: window.location.pathname.includes('/edit'),
            isDashBoard: !window.location.pathname.includes('/edit') && window.location.pathname.includes('/uarea'),
            isSearch: window.location.pathname.includes('/search')
        }

        // get the user image address.
        this.image = getProfileImage(props.profileImage)
        this.AuthService = new AuthService()
    }

    reDirect(route = '') {
        window.location.href = window.location.origin + route
    }

    handleLogout() {
        this.AuthService.logOut()
        this.reDirect()
    }

    handleEdite() {
        this.reDirect('/uarea/edit')
    }

    handleSearch() {
        this.reDirect('/search')
    }

    handleDashboard() {
        this.reDirect('/uarea')

    }

    render() {

        return (
            <>
                <div className='mobsmalltab' style={{ position: "absolute", top: this.props.top + 30, left: this.props.left - 50 }} >
                    <img className='tab-img' src={this.image} alt="" />
                    {!this.state.isSearch && <button className='mobtab-btn' onMouseDown={() => this.handleSearch()} >جست و جو</button>}
                    {!this.state.isEdit && <button className='mobtab-btn' onMouseDown={() => this.handleEdite()} >ویرایش اطلاعات</button>}
                    {!this.state.isDashBoard && <button className='mobtab-btn' onMouseDown={() => this.handleDashboard()} >ناحیه کاربری</button>}
                    <button className='mobtab-btn-exit' onMouseDown={() => this.handleLogout()} >خروج</button>
                </div>
            </>
        )
    }
}

export default SmallTab