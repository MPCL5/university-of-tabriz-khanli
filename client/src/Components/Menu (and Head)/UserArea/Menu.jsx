import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Style Region
import './Menu.css';
//Style Region End

//Component Region
import SmallTab from './UAHeadMenu/SmallTab';
//Component Region End

import AuthService from '../../../Containers/AuthService'

import caret from "../../../Assets/Images/caret.png";

class UAMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            navWidth: 0,
            zindex: -1,
            image: '',
            name: '',
            username: '',
            tabCont: ''
        }
        this.SideNavRef = React.createRef()
        this.searcinpref = React.createRef()
        this.AuthService = new AuthService()
        this.profileData = this.AuthService.getUserTokenData()
    }

    renderTabIn(e) {
        this.setState({
            tabCont: <SmallTab profileImage={localStorage.getItem('profileImage')} top={e.clientY} left={e.clientX} {...this.props} />
        })
    }

    renderTabOut() {
        this.setState({
            tabCont: ''
        })
    }

    componentWillMount() {
        document.addEventListener('mousedown', this.handleClick.bind(this), false)
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClick.bind(this), false)
    }

    open(link) {
        window.open(link)
    }
    slide() {
        if (this.state.open) {
            this.setState({
                open: false,
                navWidth: '0%',
                zindex: -1
            })
        } else {
            this.setState({
                open: true,
                navWidth: '100%',
                zindex: +1
            })
        }
    }

    handleClick(e) {
        if (this.props.backRef.current != null) {
            if (this.props.backRef.current.contains(e.target)) {
                this.setState({
                    tabCont: ''
                })
                return
            }
        } else {
            if (this.props.mobBackRef.current.contains(e.target)) {
                this.setState({
                    tabCont: ''
                })
                return
            }
        }

    }

    render() {
        return (
            <div>
                <MediaQuery minWidth={766} >
                    <div className='uahead' >
                        <p id='uabigtitle' >
                            جامعه مجازی فارغ التحصیلان دانشگاه تبریز
                        </p>
                        <p id='uasmalltitle' >
                            فعلا داریم رو این قسمت کار میکنیم...
                        </p>
                    </div>
                    <div className='uasmallmenu' >
                        <div className='uamenubox' >
                            <button className='uasearchbutton' onClick='' >
                                درباره ما
                            </button>
                            <button className='uasearchbutton' onClick={this.renderTabIn.bind(this)} >
                                <p>{this.profileData.fullName}</p>
                                <img src={caret} />
                            </button>
                        </div>
                    </div>
                    {this.state.tabCont}
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobuahead' >
                        <p id='mobuabigtitle' >
                            جامعه مجازی فارغ التحصیلان دانشگاه تبریز
                        </p>
                        <p id='mobuasmalltitle' >
                            فعلا داریم رو این قسمت کار میکنیم...
                        </p>
                    </div>
                    <div className='mobuasmallmenu' >
                        {/* <div className = 'mobuaslidebox' >
                            <span className = 'mobuamenuspan' onClick = {this.slide.bind(this)} >
                                &#9776;
                            </span>
                        </div> */}
                        <div className='uamenubox' >
                            <button className='uasearchbutton' onClick='' >
                                درباره ما
                            </button>
                            <button className='uasearchbutton' onClick={this.renderTabIn.bind(this)} >
                                {this.profileData.fullName}
                            </button>
                        </div>
                    </div>
                    {this.state.tabCont}
                </MediaQuery>
            </div>
        )
    }
}

export default UAMenu