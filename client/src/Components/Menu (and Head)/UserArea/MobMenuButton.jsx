import React, {Component} from 'react';

//Style Region
import './Menu.css'
//Sytle region End

class MobMenuButton extends Component {
    constructor(props){
        super(props)
    }
    render () {
        return (
            <div>
                <button className = 'mobuamenubutton' onClick={window.open(this.props.to)} >
                    {this.props.text}
                </button>
            </div>
        )
    }
}

export default MobMenuButton