import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Image Region
import add from '../../Assets/Images/simpline/add.png';
import ads from '../../Assets/Images/simpline/ads.png';
import biometric from '../../Assets/Images/simpline/biometric.png';
import brush from '../../Assets/Images/simpline/brush.png';
import chat from '../../Assets/Images/simpline/chat.png';
import crowd from '../../Assets/Images/simpline/crowd.png';
import eye from '../../Assets/Images/simpline/eye.png';
import find from '../../Assets/Images/simpline/find.png';
//Image Region End

//Style Region
import './Home.css'
//Style Region End

//Component Region
import Menu from '../Menu (and Head)/Menu';
import Foot from '../Foot Menu/Foot';
import { Des1, Des2, Des3, Des4, Des5, Des6, Des7, Des0 } from './Descriptions';
import { Sel1, Sel2, Sel3, Sel4, Sel5, Sel6, Sel7 } from './MobSels';
import { throwStatement } from '@babel/types';
//Component Region End

class Home extends Component {
    constructor() {
        super()
        this.state = {
            width: ['50px', '50px', '50px', '50px', '50px', '50px', '50px', '50px', '50px'],
            margin: ['10px', '10px', '10px', '10px', '10px', '10px', '10px', '10px', '10px'],
            descs: [<Des1 />, <Des2 />, <Des3 />, <Des4 />, <Des5 />, <Des6 />, <Des7 />, <Des0 />],
            idx: 7,
            infosel: [
                <Sel1 />, <Sel2 />, <Sel3 />, <Sel4 />, <Sel5 />, <Sel6 />, <Sel7 />
            ],
            mobid: 0
        }
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
        this.interval = setInterval(() => {
            this.mobDoBig()
        }, 3000)
    }
    doBig(id, out = false) {
        if (out == true) {
            this.setState({
                width: ['50px', '50px', '50px', '50px', '50px', '50px', '50px', '50px', '50px'],
                margin: ['10px', '10px', '10px', '10px', '10px', '10px', '10px', '10px', '10px'],
                idx: 7
            })
            return
        }
        var widtharr = this.state.width
        var marginarr = this.state.margin
        var i = 0
        while (i < 9) {
            if (i == id) {
                widtharr[i] = '70px'
                marginarr[i] = '0px'
            } else {
                widtharr[i] = '50px'
                marginarr[i] = '10px'
            }
            i++
        }
        this.setState({
            width: widtharr,
            margin: marginarr,
            idx: id
        })

    }
    mobDoBig() {
        var len = this.state.infosel.length
        var inid = this.state.mobid
        if (inid == len - 1) {
            inid = 0
        } else {
            inid++
        }
        this.setState({
            mobid: inid
        })
    }
    render() {
        return (
            <div>
                <Menu backRef={this.backRef} mobBackRef={this.mobBackRef} />
                <MediaQuery minWidth={766}>
                    <div className="main">
                        <div className='containor' ref={this.backRef} >
                            <div className='infoselgroup' >
                                <img className='infosel' src={crowd} alt="" style={{ width: this.state.width[0], margin: this.state.margin[0] }} onMouseOver={() => this.doBig(0)} onMouseOut={() => this.doBig(0, true)} />
                                <img className='infosel' src={chat} alt="" style={{ width: this.state.width[1], margin: this.state.margin[1] }} onMouseOver={() => this.doBig(1)} onMouseOut={() => this.doBig(1, true)} />
                                <img className='infosel' src={find} alt="" style={{ width: this.state.width[2], margin: this.state.margin[2] }} onMouseOver={() => this.doBig(2)} onMouseOut={() => this.doBig(2, true)} />
                                <img className='infosel' src={ads} alt="" style={{ width: this.state.width[3], margin: this.state.margin[3] }} onMouseOver={() => this.doBig(3)} onMouseOut={() => this.doBig(3, true)} />
                                <img className='infosel' src={biometric} alt="" style={{ width: this.state.width[4], margin: this.state.margin[4] }} onMouseOver={() => this.doBig(4)} onMouseOut={() => this.doBig(4, true)} />
                                <img className='infosel' src={brush} alt="" style={{ width: this.state.width[5], margin: this.state.margin[5] }} onMouseOver={() => this.doBig(5)} onMouseOut={() => this.doBig(5, true)} />
                                <img className='infosel' src={eye} alt="" style={{ width: this.state.width[6], margin: this.state.margin[6] }} onMouseOver={() => this.doBig(6)} onMouseOut={() => this.doBig(6, true)} />
                            </div>
                            {this.state.descs[this.state.idx] || ''}
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className="main">
                        <div ref={this.mobBackRef}>
                            <div className='mobinselfather' >
                                {this.state.infosel[this.state.mobid]}
                            </div>
                            {this.state.descs[this.state.mobid]}
                        </div>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default Home