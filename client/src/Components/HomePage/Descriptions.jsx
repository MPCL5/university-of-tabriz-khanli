import React, {Component} from 'react';

//Style Region
import './Descriptions.css'
//Style Region End

class Des0 extends Component {
    render () {
        return (
            <div className = 'desfather0' >
                <div className = 'destitle' >
                    جامعه مجازی فارغ‌التحصیلان دانشگاه تبریز
                </div>
                <div className = 'desbody' >
                    به بزرگترین انجمن مجازی فارغ‌التحصیلان دانشگاه تبریز خوش آمدید
                </div>
            </div>
        )
    }
}

class Des1 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    جمعیت فارغ‌التحصیلان
                </div>
                <div className = 'desbody' >
                    می‌توانید با ثبت نام و ورود به بخش کاربری خود به جمعیت بزرگی از فارغ‌التحصیلان دانشگاه تبریز بپیوندید و با آن‌ها ارتباط برقرار کنید
                </div>
            </div>
        )
    }
}

class Des2 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    پیام دادن به دوستان
                </div>
                <div className = 'desbody' >
                    میتوانید به صورت خصوصی و عمومی برای دوستان خود پیام ارسال کنید    
                </div>
            </div>
        )
    }
}

class Des3 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    سیستم جست و جوی حرفه‌ای
                </div>
                <div className = 'desbody' >
                    با استفاده از سیستم جست و جوی حرفه‌ای می‌توانید در میان انبوهی از پیام‌ها، پست‌ها و افراد حقیقی جست و جو کنید
                </div>
            </div>
        )
    }
}

class Des4 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    پیام رسان باشید!
                </div>
                <div className = 'desbody' >
                    اخبار و اتفاقات اطراف خود را با دیگران به اشتراک بگذارید
                </div>
            </div>
        )
    }
}

class Des5 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    امنیت
                </div>
                <div className = 'desbody' >
                    اطلاعات شما با امنیت سطح بالا در سطح وب منتقل و نگه داری می‌شوند، همچنین تدابیر امنیتی این امکان را به شما میدهند که اطلاعات خود را در کسری از دقیقه امحاء کنید
                </div>
            </div>
        )
    }
}

class Des6 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    شخصی سازی!
                </div>
                <div className = 'desbody' >
                    فضای کاربری خود را با توجه به سلایق خود شخصی سازی کنید 
                </div>
            </div>
        )
    }
}

class Des7 extends Component {
    render () {
        return (
            <div className = 'desfather' >
                <div className = 'destitle' >
                    در نگاه عموم
                </div>
                <div className = 'desbody' >
                    اطلاعات خود را وارد کنید، فضای خود را ایجاد کرده و خود را در دید عموم مردم قرار دهید
                </div>
            </div>
        )
    }
}

export {
    Des0,
    Des1,
    Des2,
    Des3,
    Des4,
    Des5,
    Des6,
    Des7
}