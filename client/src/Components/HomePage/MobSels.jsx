import React, {Component} from 'react';

//Style region
import './Home.css';
//Style Region End

//Image Region
import add from '../../Assets/Images/simpline/add.png';
import ads from '../../Assets/Images/simpline/ads.png';
import biometric from '../../Assets/Images/simpline/biometric.png';
import brush from '../../Assets/Images/simpline/brush.png';
import chat from '../../Assets/Images/simpline/chat.png';
import crowd from '../../Assets/Images/simpline/crowd.png';
import eye from '../../Assets/Images/simpline/eye.png';
import find from '../../Assets/Images/simpline/find.png';
//Image Region End

class Sel1 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {crowd}  alt = '' />
            </div>
        )
    }
}

class Sel2 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {chat}  alt = '' />
            </div>
        )
    }
}

class Sel3 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {find}  alt = '' />
            </div>
        )
    }
}

class Sel4 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {ads}  alt = '' />
            </div>
        )
    }
}

class Sel5 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {biometric}  alt = '' />
            </div>
        )
    }
}

class Sel6 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {brush}  alt = '' />
            </div>
        )
    }
}

class Sel7 extends Component {
    render () {
        return (
            <div>
                <img className = 'mobinsel' src = {eye}  alt = '' />
            </div>
        )
    }
}

export {
    Sel1,
    Sel2,
    Sel3,
    Sel4,
    Sel5,
    Sel6,
    Sel7
}