import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

import './Profile.css';

//Image Region
import phone from '../../Assets/Images/phone.png';
import instagram from '../../Assets/Images/instagram.png';
import twitter from '../../Assets/Images/twitter.png';
import telegram from '../../Assets/Images/telegram.png';
//Image Region End

import Menu from '../Menu (and Head)/Menu';
import FriendsArea from './FriendsArea'

import profileRequest from '../../Containers/Profile';
import { getFriends } from '../../Containers/FriendService'
import { getProfileImage } from '../../Containers/Profile'

class Profile extends Component {
    constructor(props) {
        super(props)
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
    }

    state = {
        username: '',
        fullName: '',
        userimage: '',
        shortBio: '',
        bio: '',
        educations: '',
        job: '',
        phone: '',
        telegram: '',
        instagram: '',
        twitter: '',
        friends: []
    }

    componentWillMount() {
        profileRequest(this.props.match.params.userid)
            .then(result => {
                if (result.error) return this.setState({ error: result.error })
                this.setState(pervState => ({ ...pervState, ...result }))

                getFriends(this.props.match.params.userid, 3)
                    .then(results => {
                        this.setState({ friends: results.results })
                        console.log("result", this.state.friends);

                    })
                console.log(this.state);
            })
            .catch(error => this.setState({ error: error }))
    }


    render() {
        const imageSrc = getProfileImage(this.state.profileImage)

        return (
            <>
                <Menu backRef={this.backRef} mobBackRef={this.mobBackRef} />
                <MediaQuery minWidth={765} >
                    <div className='profile-all-screen-show' ref={this.backRef}>
                        <div className='profile-view' >
                            <img src={imageSrc} className='profile-image' alt="" />
                            <div className='profile-name-father' >
                                <p id='profile-name' >
                                    {this.state.fullName}
                                </p>
                                <p id='profile-id'>
                                    @{this.state.username}
                                </p>
                            </div>
                            <div className='profile-bio' >
                                <p id='bio-small' >
                                    {this.state.shortBio}
                                </p>
                                <p id='bio-full' >
                                    {this.state.bio}
                                </p>
                            </div>
                        </div>
                        <div className='profile-other-father' >
                            <div className='profile-other-fchild' id='info' >
                                <p className='profile-other-def' >اطلاعات عمومی</p>
                                <div className='profile-other-mini' >
                                    <p className='profile-other-mini-child-p1' >
                                        تحصیلات
                                        </p>
                                    <p className='profile-other-mini-child-p2' >
                                        {this.state.educations}
                                    </p>
                                </div>
                                <div className='profile-other-mini' >
                                    <p className='profile-other-mini-child-p1' >
                                        شغل
                                        </p>
                                    <p className='profile-other-mini-child-p2' >
                                        {this.state.job}
                                    </p>
                                </div>
                            </div>
                            <div className='profile-other-fchild' id='connect' >
                                <p className='profile-other-def' >ارتباط با من</p>
                                <div className='profile-other-mini' >
                                    <img className='connect-child' src={phone} alt="" />
                                    <img className='connect-child' src={instagram} alt="" />
                                    <img className='connect-child' src={twitter} alt="" />
                                    <img className='connect-child' src={telegram} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="profile-friends">
                            <div className="profile-friends-title">:دوستان</div>
                            <div className="profile-friends-members">
                                <FriendsArea results={this.state.friends} />
                            </div>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div ref={this.mobBackRef}>
                        <div className='mobprofile-all-screen-show' ref={this.backRef}>
                            <div className='mobprofile-view' >
                                <img src={imageSrc} className='mobprofile-image' alt="" />
                                <div className='mobprofile-name-father' >
                                    <p id='mobprofile-name' >
                                        {this.state.fullName}
                                    </p>
                                    <p id='mobprofile-id'>
                                        @{this.state.username}
                                    </p>
                                </div>
                                <div className='mobprofile-bio' >
                                    <p id='mobbio-small' >
                                        {this.state.shortBio}
                                    </p>
                                    <p id='mobbio-full' >
                                        {this.state.bio}
                                    </p>
                                </div>
                            </div>
                            <div className='mobprofile-other-father' >
                                <div className='mobprofile-other-fchild' id='info' >
                                    <p className='mobprofile-other-def' >اطلاعات عمومی</p>
                                    <div className='mobprofile-other-mini' >
                                        <p className='mobprofile-other-mini-child-p1' >
                                            تحصیلات
                                        </p>
                                        <p className='mobprofile-other-mini-child-p2' >
                                            {this.state.educations}
                                        </p>
                                    </div>
                                    <div className='mobprofile-other-mini' >
                                        <p className='mobprofile-other-mini-child-p1' >
                                            شغل
                                        </p>
                                        <p className='mobprofile-other-mini-child-p2' >
                                            {this.state.job}
                                        </p>
                                    </div>
                                </div>
                                <div className='mobprofile-other-fchild' id='connect' >
                                    <p className='mobprofile-other-def' >ارتباط با من</p>
                                    <div className='mobprofile-other-mini' >
                                        <img className='connect-child' src={phone} alt="" />
                                        <img className='connect-child' src={instagram} alt="" />
                                        <img className='connect-child' src={twitter} alt="" />
                                        <img className='connect-child' src={telegram} alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </MediaQuery>
            </>
        )
    }
}

export default Profile