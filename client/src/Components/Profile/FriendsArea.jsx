import React, { Component } from 'react'

import Member from '../Friends/Member'

class FriendsArea extends Component {
    constructor(props) {
        super(props)

        this.state = {
            results: props.results
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        return {
            ...prevState,
            ...nextProps
        }
    }

    render() {
        console.log("state is: ", this.state.results);

        return (
            <>
                {this.state.results.map(value =>
                    (
                        <div className="profile-Friends-member" key={value.username} >
                            <Member name={value.fullName} field={value.field} yearOfJoin={value.yearOfJoin} profileImage={value.profileImage} />
                        </div>
                    )
                )}
            </>
        )
    }
}

export default FriendsArea