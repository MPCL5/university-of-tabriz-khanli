import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

import './UAHeadMenu.css';

import masoud from '../../../Assets/Images/masoud.jpg';

class SmallTab extends Component{
    constructor (props){
        super (props)
        this.state = {
            isEdit: window.location.pathname.includes('/edit')
        }
    }
    render () {
        return (
            <div className = 'mobsmalltab' style = {{position: "absolute", top: this.props.top + 20, left: this.props.left - 50}} >
                {/* <img className = 'tab-img' src={masoud} alt=""/> */}
                <button className = 'mobtab-btn' >جست و جو</button>
                {!this.state.isEdit ? <button className = 'mobtab-btn' >ویرایش اطلاعات</button> : ''}
                <button className = 'mobtab-btn-exit' onMouseDown = { () => console.log('ko')} >خروج</button>
            </div>
        )
    }
}

export default SmallTab