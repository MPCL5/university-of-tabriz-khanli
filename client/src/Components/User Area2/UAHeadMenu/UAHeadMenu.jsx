import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

import './UAHeadMenu.css';
import SmallTab from './SmallTab'; 

import masoud from '../../../Assets/Images/masoud.jpg';
import { bool } from 'prop-types';



class UAHeadMenu extends Component {
    constructor (props) {
        super (props)
        this.state = {
            image: '',
            name: '',
            username: '',
            tabCont: ''
        }
        this.backRef = this.props.backRef
    }

    renderTabIn (e) {
        this.setState ({
            tabCont: <SmallTab top = {e.clientY} left = {e.clientX} />
        })
    }

    renderTabOut (){
        this.setState ({
            tabCont: ''
        })
    }

    componentWillMount () {
        document.addEventListener('mousedown', this.handleClick.bind(this), false)
    }
    componentWillUnmount () {
        document.removeEventListener('mousedown', this.handleClick.bind(this), false)
    }

    handleClick (e) {
        if (this.props.backRef.current != null){
            if (this.props.backRef.current.contains(e.target)){
                this.setState ({
                    tabCont: ''
                })
                return
            }
        }else{
            if (this.props.mobBackRef.current.contains(e.target)){
                this.setState ({
                    tabCont: ''
                })
                return
            }
        }
        
    }

    render () {
        return (
            <div>
            <MediaQuery minWidth = {766} >
                <div className = 'uamenu-father'>
                    <img className = 'uamenu-father-img' src={masoud} alt="" onClick = {this.renderTabIn.bind(this)} />
                </div>
                {this.state.tabCont}
            </MediaQuery>
            <MediaQuery minWidth = {331} maxWidth = {765} >
                <div className = 'mobuamenu-father' >
                    <img className = 'uamenu-father-img' src={masoud} alt="" onClick = {this.renderTabIn.bind(this)} />
                </div>
                {this.state.tabCont}
            </MediaQuery>
            </div>
        )
    }
}



export default UAHeadMenu