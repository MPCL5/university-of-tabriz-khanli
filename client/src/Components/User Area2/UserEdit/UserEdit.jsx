import React, {Component} from 'react'; 
import MediaQuery from 'react-responsive';

import UAHeadMenu from '../UAHeadMenu/UAHeadMenu';

import phone from '../../../Assets/Images/phone.png';
import instagram from '../../../Assets/Images/instagram.png';
import twitter from '../../../Assets/Images/twitter.png';
import telegram from '../../../Assets/Images/telegram.png';
import camera from '../../../Assets/Images/signs/camera.png';

import './UserEdit.css';

class UserEdit extends Component {
    constructor () {
        super()
        this.state = {
            username: '',
            fullname: '',
            userimage: '',
            biosmall: '',
            biofull: '',
            educs: '',
            job: '',
            phone: '',
            telegram: '',
            instagram: '',
            twitter:'',
            usernamestyle: ''
        }
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
        this.userNameRef = React.createRef()
    }

    avatarJob (files) {
        var file = URL.createObjectURL(files[0])
        this.setState ({
            userimage: file
        })
    }

    render () {
        return (
            <div className = 'edit-all' >    
                <UAHeadMenu backRef = {this.backRef} mobBackRef = {this.mobBackRef} />    
                <MediaQuery minWidth = {766}>
                    <div className = 'edit-father' ref = {this.backRef} >
                        <div className = 'edit-def-father' >
                            <p className = 'edit-big-def' >
                                ویرایش اطلاعات
                            </p>
                            <p className = 'edit-small-def' >
                                می‌توانید اطلاعات خود را از اینجا ویرایش کنید
                            </p>
                        </div>
                        <label for='userav' className = 'avatarspan' >
                                    <img className = 'avimage' src={this.state.userimage || camera} alt=""/>
                                    <input id='userav' className = 'avatarinp' type="file" onChange = {(e) => {
                                        var file = URL.createObjectURL(e.target.files[0])
                                        this.setState ({
                                            userimage: file
                                        })
                                    }} />
                                    <p className = 'avatarlabel' >
                                        تصویر پروفایل:
                                    </p>
                        </label>
                        <div className = 'edit-area-father' >
                            {/* getting usee image */}
                            
                            <div className = 'edit-main-father' >
                                <div className = 'main-child' >
                                    <p className = 'main-child-def' >
                                        نام کاربری
                                    </p>
                                    <input className = 'child-input' placeholder = '@' style = {{border: this.state.usernamestyle, direction: 'ltr'}} type="text" maxLength = {100} onChange = {(e) => {
                                        //check if username is valid
                                        var ok = true
                                        if (!ok){
                                            this.setState({username: e.target.value,usernamestyle: 'solid 1px red'})
                                        }else{
                                            this.setState({username: '',usernamestyle: 'solid 1px green'})
                                        }
                                    }} />
                                </div>
                                <div className = 'main-child' >
                                    <p className = 'main-child-def' >
                                        بیوگرافی کوتاه
                                    </p>
                                    <input className = 'child-input' type="text" maxLength = {100} onChange = {(e) => this.setState({biosmall: e.target.value})} />
                                </div>
                                <div className = 'main-child' >
                                    <p className = 'main-child-def' >
                                        بیوگرافی
                                    </p>
                                    <input className = 'child-input' type="text" maxLength = {300} onChange = {(e) => this.setState({biofull: e.target.value})} />
                                </div>
                                <div className = 'main-child' >
                                        <p className = 'main-child-def' >
                                            شغل
                                        </p>
                                        <input className = 'child-input' type="text" maxLength = {200} onChange = {(e) => this.setState({job: e.target.value})} />
                                    </div>
                                    <div className = 'main-child' >
                                        <p className = 'main-child-def' >
                                            تحصیلات
                                        </p>
                                        <input className = 'child-input' type="text" maxLength = {300} onChange = {(e) => this.setState({educs: e.target.value})} />
                                </div>
                            </div>
                            <div className = 'edit-extra-father' >
                                <div className = 'edit-extra-connect' >
                                    <div className = 'main-child' >
                                        <img className = 'extra-connect-image' src={phone} alt=""/>
                                        <input className = 'child-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({phone: e.target.value})} />
                                    </div>
                                    <div className = 'main-child' >
                                        <img className = 'extra-connect-image' src={instagram} alt=""/>
                                        <input className = 'child-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({instagram: e.target.value})} />
                                    </div>
                                    <div className = 'main-child' >
                                        <img className = 'extra-connect-image' src={twitter} alt=""/>
                                        <input className = 'child-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({twitter: e.target.value})} />
                                    </div>
                                    <div className = 'main-child' >
                                        <img className = 'extra-connect-image' src={telegram} alt=""/>
                                        <input className = 'child-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({telegram: e.target.value})} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className = 'edit-btn-group' >
                            <button className = 'edit-back-button' >
                                انصراف
                            </button>
                            <button className = 'edit-submit-button' >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth = {331} maxWidth = {765} >
                <div className = 'mobedit-father' ref = {this.mobBackRef} >
                        <div className = 'mobedit-def-father' >
                            <p className = 'mobedit-big-def' >
                                ویرایش اطلاعات
                            </p>
                            <p className = 'mobedit-small-def' >
                                می‌توانید اطلاعات خود را از اینجا ویرایش کنید
                            </p>
                        </div>
                        <div className = 'mobedit-area-father' >
                            {/* getting usee image */}
                            <div className = 'mobedit-main-father' >
                                <div className = 'mobmain-child' >
                                    <p className = 'mobmain-child-def' >
                                        نام کاربری
                                    </p>
                                    <input className = 'mobchild-input' placeholder = '@' style = {{border: this.state.usernamestyle, direction: 'ltr'}} type="text" maxLength = {100} onChange = {(e) => {
                                        //check if username is valid
                                        var ok = true
                                        if (!ok){
                                            this.setState({username: e.target.value,usernamestyle: 'solid 1px red'})
                                        }else{
                                            this.setState({username: '',usernamestyle: 'solid 1px green'})
                                        }
                                    }} />
                                </div>
                                <div className = 'mobmain-child' >
                                    <p className = 'mobmain-child-def' >
                                        بیوگرافی کوتاه
                                    </p>
                                    <input className = 'mobchild-input' type="text" maxLength = {100} onChange = {(e) => this.setState({biosmall: e.target.value})} />
                                </div>
                                <div className = 'mobmain-child' >
                                    <p className = 'mobmain-child-def' >
                                        بیوگرافی
                                    </p>
                                    <input className = 'mobchild-input' type="text" maxLength = {300} onChange = {(e) => this.setState({biofull: e.target.value})} />
                                </div>
                                <div className = 'mobmain-child' >
                                        <p className = 'mobmain-child-def' >
                                            شغل
                                        </p>
                                        <input className = 'mobchild-input' type="text" maxLength = {200} onChange = {(e) => this.setState({job: e.target.value})} />
                                    </div>
                                    <div className = 'mobmain-child' >
                                        <p className = 'mobmain-child-def' >
                                            تحصیلات
                                        </p>
                                        <input className = 'mobchild-input' type="text" maxLength = {300} onChange = {(e) => this.setState({educs: e.target.value})} />
                                </div>
                            </div>
                            <div className = 'mobedit-extra-father' >
                                <div className = 'mobedit-extra-connect' >
                                    <div className = 'mobmain-child' >
                                        <img className = 'mobextra-connect-image' src={phone} alt=""/>
                                        <input className = 'mobchild-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({phone: e.target.value})} />
                                    </div>
                                    <div className = 'mobmain-child' >
                                        <img className = 'mobextra-connect-image' src={instagram} alt=""/>
                                        <input className = 'mobchild-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({instagram: e.target.value})} />
                                    </div>
                                    <div className = 'mobmain-child' >
                                        <img className = 'mobextra-connect-image' src={twitter} alt=""/>
                                        <input className = 'mobchild-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({twitter: e.target.value})} />
                                    </div>
                                    <div className = 'mobmain-child' >
                                        <img className = 'mobextra-connect-image' src={telegram} alt=""/>
                                        <input className = 'mobchild-input' type="text" maxLength = {200} style = {{direction: 'ltr'}} onChange = {(e) => this.setState({telegram: e.target.value})} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className = 'mobedit-btn-group' >
                            <button className = 'mobedit-back-button' >
                                انصراف
                            </button>
                            <button className = 'mobedit-submit-button' >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default UserEdit