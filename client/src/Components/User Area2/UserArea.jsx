import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

// Style Region
import './UserArea.css';
// Style Region End

// Component Region
import UAHeadMenu from './UAHeadMenu/UAHeadMenu';
// Component Region End

// Image Region
import masoud from '../../Assets/Images/masoud.jpg';
import phone from '../../Assets/Images/phone.png';
import instagram from '../../Assets/Images/instagram.png';
import twitter from '../../Assets/Images/twitter.png';
import telegram from '../../Assets/Images/telegram.png';
// Image Region End

// Contanier Region
import withAuth from '../../Containers/WithAuth'
// Contanier Region End

class USerArea extends Component {
    constructor () {
        super()
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
    }
    render () {
        return (
            <div style = {{minHeight: '500px'}}>
            <UAHeadMenu backRef = {this.backRef} mobBackRef = {this.mobBackRef} />
            <MediaQuery minWidth = {766} >
                <div className = 'area-father' ref = {this.backRef} >
                    <div className = 'area-me-father' >
                        {/* <img className = 'area-me-img' src={masoud} alt=""/> */}
                        <p className = 'area-me-name' >
                            مسعود پورغفار اقدم
                        </p>
                        <p className = 'area-me-username' >
                            @mpcl5x
                        </p>
                        <p className = 'area-me-sbio' >
                            ما زنده به آنیم که آرام نگیریم
                        </p>
                        <p className = 'area-me-fbio' >
                            مسعود پورغفار اقدم هستم، دانشجوی مهندسی کامپیوتر دانشگاه تبریز
                            اخیرا هم دارم رو این پروژه که میبینید کار میکنم
                        </p>
                        <div className = 'area-me-extra-father' >
                            <div className = 'area-me-extra-child' >
                                <p className = 'extra-key' > 
                                    شغل
                                </p>
                                <p className = 'extra-value' >
                                    بیکار
                                </p>
                            </div>
                        </div>
                        <div className = 'area-me-extra-father' >
                            <div className = 'area-me-extra-child' >
                                <p className = 'extra-key' > 
                                    تحصیلات
                                </p>
                                <p className = 'extra-value' >
                                    کارشناسی دانشگاه شهید بهشتی<br/>
                                    ارشد علم و صنعت<br/>
                                    دکتری علم و صنعت
                                </p>
                            </div>
                        </div>
                        <div className = 'area-me-connect-father' >
                            <img className = 'connect-child' src = {phone} alt=""/>
                            <img className = 'connect-child' src = {instagram} alt=""/>
                            <img className = 'connect-child' src = {twitter} alt=""/>
                            <img className = 'connect-child' src = {telegram} alt=""/>
                        </div>
                        <button className = 'edit-btn' onClick = {() => window.open('/uarea/edit')} >
                            ویرایش اطلاعات
                        </button>
                    </div>
                    <div className = 'area-maybe' >
                        <p className = 'area-maybe-title' >
                            افرادی که شاید بشناسید
                        </p>
                        <div className = 'area-maybe-other' >

                        </div>
                    </div>
                </div>
            </MediaQuery>
            <MediaQuery minWidth = {331} maxWidth = {765} >
                <div className = 'mobarea-father' ref = {this.mobBackRef} >
                    <div className = 'mobarea-me-father' >
                        {/* <img className = 'area-me-img' src={masoud} alt=""/> */}
                        <p className = 'mobarea-me-name' >
                            مسعود پورغفار اقدم
                        </p>
                        <p className = 'mobarea-me-username' >
                            @mpcl5x
                        </p>
                        <p className = 'mobarea-me-sbio' >
                            ما زنده به آنیم که آرام نگیریم
                        </p>
                        <p className = 'mobarea-me-fbio' >
                            مسعود پورغفار اقدم هستم، دانشجوی مهندسی کامپیوتر دانشگاه تبریز
                            اخیرا هم دارم رو این پروژه که میبینید کار میکنم
                        </p>
                        <div className = 'mobarea-me-extra-father' >
                            <div className = 'mobarea-me-extra-child' >
                                <p className = 'mobextra-key' > 
                                    شغل
                                </p>
                                <p className = 'mobextra-value' >
                                    بیکار
                                </p>
                            </div>
                        </div>
                        <div className = 'mobarea-me-extra-father' >
                            <div className = 'mobarea-me-extra-child' >
                                <p className = 'mobextra-key' > 
                                    تحصیلات
                                </p>
                                <p className = 'mobextra-value' >
                                    کارشناسی دانشگاه شهید بهشتی<br/>
                                    ارشد علم و صنعت<br/>
                                    دکتری علم و صنعت
                                </p>
                            </div>
                        </div>
                        <div className = 'mobarea-me-connect-father' >
                            <img className = 'mobconnect-child' src = {phone} alt=""/>
                            <img className = 'mobconnect-child' src = {instagram} alt=""/>
                            <img className = 'mobconnect-child' src = {twitter} alt=""/>
                            <img className = 'mobconnect-child' src = {telegram} alt=""/>
                        </div>
                        <button className = 'mobedit-btn' onClick = {() => window.open('/uarea/edit')} >
                            ویرایش اطلاعات
                        </button>
                    </div>
                    <div className = 'mobarea-maybe' >
                        <p className = 'mobarea-maybe-title' >
                            افرادی که شاید بشناسید
                        </p>
                        <div className = 'mobarea-maybe-other' >

                        </div>
                    </div>
                </div>
            </MediaQuery>
            </div>
        )
    }
}

export default withAuth(USerArea, true)