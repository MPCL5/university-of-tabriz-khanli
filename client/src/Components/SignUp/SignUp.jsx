import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

// Style Region
import './SignUp.css'
// Style Region End

// Component Region
import Menu from '../Menu (and Head)/Menu';
import Step1 from './Steps/Step1';
import Step2 from './Steps/Step2';
import Step3 from './Steps/Step3';
// Component Region End

// Image Region
// Image Region End

// Container Region
import withAuth from '../../Containers/WithAuth'
// Container Region End

class SignUp extends Component {

    constructor () {
        super ()
        this.state = {
            ncode: null,
            profileimage: null,
            email: null,
            username: null,
            bio: null,
            educs: null,
            work: null,
            phone: null,
            instagram: null,
            twitter: null,
            telegram: null,
            stepid: 0,
            error : ''
        }
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
    }

    // componentDidMount() {
    //     this.setState({ steplist : [<Step1 whenok = {this.step1ok.bind(this)} error = {this.state.error} />, <Step2 whenok = {this.step2ok.bind(this)} />, <Step3 whenok = {this.step3ok.bind(this)} />] })
    //     // this.steplist = [<Step1 whenok = {this.step1ok.bind(this)} error = {this.state.error} />, <Step2 whenok = {this.step2ok.bind(this)} />, <Step3 whenok = {this.step3ok.bind(this)} />]
    // }

    step1ok (nxcode) {
        this.setState({
            ncode: nxcode,
            stepid: 1
        })
    }

    step2ok (username, password) {
        // do not forget to replace password with its hash value
        this.setState ({
            username: username,
            password: password
        })
        setTimeout(() => {
            this.setState ({
                stepid: 2
            })
        }, 550);
    }

    step3ok (sttdict) {
        // use state and submit information to server
        // after submition redirect to login page0
        window.location.href = window.location.origin + '/uarea'
    }

    render () {
        return (
            <div>
                <Menu backRef = {this.backRef} mobBackRef = {this.backRef} />
                <div className = 'containall' ref = {this.backRef} >
                    {/* {this.steplist[this.state.stepid]} */}
                    {(() => {
                        switch(this.state.stepid) {
                            case 0:                                
                                return <Step1 whenok = {this.step1ok.bind(this)} error = {this.state.error} />
                            case 1:
                                return <Step2 whenok = {this.step2ok.bind(this)} {...this.props} />
                            case 2:
                                return <Step3 whenok = {this.step3ok.bind(this)} username={this.state.username} password={this.state.password} />
                        }
                    })()}
                </div>
            </div>
        )
    }
}

export default withAuth(SignUp, false)