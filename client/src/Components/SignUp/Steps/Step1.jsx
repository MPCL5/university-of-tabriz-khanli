import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

// Style Region
// Style Region End

// Component Region
import './Step1.css'
import { string } from 'postcss-selector-parser';
import Message from '../../Message Area/Message'
// Component Region End

// Image Region 
// Image Region End

import { registerStepOne as stepOne } from '../../../Containers/AccountService'

class Step1 extends Component {
    constructor (props) {
        super (props) 
        this.state = {
            propError : this.props.error,
            error: 'heigthDown 500ms 0ms forwards',
            moberror: 'mobheigthDown 500ms 0ms forwards',
            contEn: true,
            animation: 'divIn 500ms 500ms forwards',
            mobanimation: 'mobdivIn 500ms 500ms forwards'
        }
        
        this.ncode = ''
        this.inpref = React.createRef ()
        this.yearinpref = React.createRef ()
        this.mobinpref = React.createRef ()
        this.mobyearinpref = React.createRef ()
    }

    componentDidMount() {
        this.props.error && this.showError(true, this.state.propError)

        console.log(this.state);

    }

    checkIdentity(identity) {
        let sum = 0
        identity = identity.split('').reverse()
    
        for(let i=2 ; i<identity.length+1 ; i++) {
            sum += (i*Number(identity[i-1]))     
        }
        
        const mod = sum % 11
        if(mod < 2) {
            if(mod == Number(identity[0])) 
                return true
            else
                return false
        } else {
            if((11-mod) == Number(identity[0]))
                return true
            else
                return false
        }
    }

    checkifOK (value) {
        if (this.state.propError)
        value = value.split('')
        this.showError(false)

        // check value conditions.
        if (value.length != 10){
            //this.showError(false)
            this.setState ({contEn: true})

            if (value.length != 0)
                return this.showError(true,'طول کد ملی وراد شده نباید کمتر از 10 باشد') 
        }

        if(this.checkIdentity(value.join(''))) {
            // this.setState ({
            //     contEn: false,
            //     animation: 'divOut 500ms 0ms forwards',
            //     mobanimation: 'mobdivOut 500ms 0ms forwards'
            // })
            // TODO: add api verify
            stepOne(value.join(''), 97)
                .then(result => {
                    if (result.valid) {
                        this.props.whenok (value.join(''))
                    } else {
                        this.showError(true, 'کد ملی شما موجود نمی باشد')
                        console.log(this.state);
                        
                    }
                })
            
        }
        else {
            this.showError(true,'کد ملی وارد شده معتبر نمی‌باشد')
        }
    }

    showError (boolx, bodyx = null) {
        if (boolx){
            this.setState({
                error: 'heigthUp 500ms 0ms forwards',
                moberror: 'mobheigthUp 500ms 0ms forwards',
                propError: bodyx
            })
        }else{
            this.setState({
                error: 'heigthDown 500ms 0ms forwards',
                moberror: 'mobheigthDown 500ms 0ms forwards',
                propError: ''
            })
        }
    }

    render () {
        return (
            <div>  
                <MediaQuery minWidth = {766} >
                    <div className = 'stepbox' style = {{animation: this.state.animation}} >
                        <div className = 'stepdef' >
                            <p className = 'stepdefbig' >
                                کد ملی
                            </p>
                            <p className = 'stepdefsmall' >
                                برای شروع ثبت نام ابتدا کد ملی خود را وارد کنید
                            </p>
                        </div>
                        <div className = 'stepbody' >
                            <input className = 'stepyearinp' type="text" placeholder = 'سال ورود' ref = {this.yearinpref} />
                            <input className = 'stepinput' type="text" placeholder = 'کد ملی خود را وارد کنید' ref = {this.inpref} onChange = { (e) => this.checkifOK(e.target.value)} maxLength = '10' />
                            <div className = 'steperror' style = {{animation: this.state.error}} >{this.state.propError}</div>
                        </div>
                        <div className = 'stepcontrol' >
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth = {331} maxWidth = {765} >
                <div className = 'mobstepbox' style = {{animation: this.state.mobanimation}} >
                        <div className = 'mobstepdef' >
                            <p className = 'mobstepdefbig' >
                                کد ملی
                            </p>
                            <p className = 'mobstepdefsmall' >
                                برای شروع ثبت نام ابتدا کد ملی خود را وارد کنید
                            </p>
                        </div>
                        <div className = 'mobstepbody' >
                            <input className = 'stepyearinp' type="text" placeholder = 'سال ورود' ref = {this.mobyearinpref} />
                            <input className = 'mobstepinput' type="text" placeholder = 'کد ملی خود را وارد کنید' ref = {this.mobinpref} onChange = { (e) => this.checkifOK(e.target.value)} maxLength = '10' />
                            <div className = 'mobsteperror' style = {{animation: this.state.moberror}} >{this.state.errorbody}</div>
                        </div>
                        <div className = 'mobstepcontrol' >
                        </div>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default Step1