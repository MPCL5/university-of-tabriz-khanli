import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

// Style Region
import './Step1.css'
// Style Region End

// Component Region
import Message from '../../Message Area/Message'
// Component Region End

// Image Region 
// Image Region End

// Container Regoin
import { registerStepTwo } from '../../../Containers/AccountService'
import AuthService from '../../../Containers/AuthService'
// Container Regoin End

class Step2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            errorMessage: '',
            mail_error: 'heigthDown 500ms 0ms forwards',
            username_error: 'heigthDown 500ms 0ms forwards',
            password_error: 'heigthDown 500ms 0ms forwards',
            mobmail_error: 'mobheigthDown 500ms 0ms forwards',
            mobusername_error: 'mobheigthDown 500ms 0ms forwards',
            mobpassword_error: 'mobheigthDown 500ms 0ms forwards',
            mail_errorbody: null,
            username_errorbody: null,
            password_errorbody: null,
            contEn: true,
            animation: 'divIn 500ms 0ms forwards',
            mobanimation: 'mobdivIn 500ms 0ms forwards',
            avatar: null
        }
        this.email = null
        this.username = null
        this.password = null

        this.mail_ok = false
        this.user_ok = false
        this.pass_ok = false

        this.Auth = new AuthService()

        this.handleSubmite = this.handleSubmite.bind(this)
    }

    mailok(value) {
        this.mail_ok = false
        value = value.split('')
        if (value.length >= 4) {
            var has = false
            for (var i = 0; i < value.length; i++) {
                if (value[i] == '@') {
                    has = true
                }
            }
            if (!has) {
                this.showmailError(true, 'پست الکترونیک وارد شده معتبر نیست')
                this.mail_ok = false
            } else {
                this.showmailError(false)
                this.email = value
                this.mail_ok = true
            }
        } else {
            this.showmailError(false)
            this.email = value
            this.mail_ok = true
        }
        this.allok()
    }

    avatarJob(files) {
        var file = URL.createObjectURL(files[0])
        this.setState({
            avatar: file
        })
    }

    usernameok(value) {
        // check if username exists in database or not and show error or not
        if (value.split('').length == 0) {
            this.user_ok = false

        } else {
            this.username = value
            this.user_ok = true
        }
        this.allok()
    }

    passwordok(value) {
        if (value.length < 8 && value.length != 0) {
            this.showpasswordError(true, 'طول رمز وارد شده کمتر از حد مجاز می‌باشد')
            this.pass_ok = false
        } else {
            this.showpasswordError(false)
            this.password = value
            this.pass_ok = true
        }
        if (value.length == 0) {
            this.showpasswordError(false)
            this.password = value
            this.pass_ok = true
        }
        this.allok()
    }

    showmailError(boolx, bodyx = null) {
        if (boolx) {
            this.setState({
                mail_error: 'heigthUp 500ms 0ms forwards',
                mobmail_error: 'mobheigthUp 500ms 0ms forwards',
                mail_errorbody: bodyx
            })
        } else {
            this.setState({
                mail_error: 'heigthDown 500ms 0ms forwards',
                mobmail_error: 'mobheigthDown 500ms 0ms forwards',
                mail_errorbody: null
            })
        }
    }

    showusernameError(boolx, bodyx = null) {
        if (boolx) {
            this.setState({
                username_error: 'heigthUp 500ms 0ms forwards',
                mobusername_error: 'mobheigthUp 500ms 0ms forwards',
                username_errorbody: bodyx
            })
        } else {
            this.setState({
                username_error: 'heigthDown 500ms 0ms forwards',
                mobusername_error: 'mobheigthDown 500ms 0ms forwards',
                username_errorbody: null
            })
        }
    }

    showpasswordError(boolx, bodyx = null) {
        if (boolx) {
            this.setState({
                password_error: 'heigthUp 500ms 0ms forwards',
                mobpassword_error: 'mobheigthUp 500ms 0ms forwards',
                password_errorbody: bodyx
            })
        } else {
            this.setState({
                password_error: 'heigthDown 500ms 0ms forwards',
                mobpassword_error: 'mobheigthDown 500ms 0ms forwards',
                password_errorbody: null
            })
        }
    }

    allok() {
        if (this.user_ok && this.pass_ok && this.mail_ok) {
            this.setState({
                contEn: false
            })
        } else {
            this.setState({
                contEn: true
            })
        }
    }

    handleSubmite() {
        console.log(this.username, this.email, this.password);

        registerStepTwo(this.username, this.email.join(''), this.password)
            .then(result => {
                console.log(result);

                if (result.error)
                    return this.setState({ errorMessage: result.error })

                this.props.whenok(this.username, this.password)
                
                
            })
            .catch(error => {
                console.log(error);
                // this.setState({ errorMessage: error })
            })
    }

    render() {
        return (
            <div >
                <MediaQuery minWidth={766} >
                    <div className='stepbox' style={{ animation: this.state.animation }} >
                        <div className='stepdef' >
                            <p className='stepdefbig' >
                                اطلاعات خصوصی
                            </p>
                            <p className='stepdefsmall' >
                                ،{this.props.fullname} عزیز
                                <br />
                                پست الکترونیک، ایمیل و رمز عبور خود را تعیین کنید
                            </p>
                        </div>
                        <div className='stepbody' >
                            {this.state.errorMessage && <Message type="error">{this.state.errorMessage}</Message>}                            <input className='stepinput' type="text" placeholder='پست الکترونیک' onChange={(e) => this.mailok(e.target.value)} />
                            <div className='steperror' style={{ animation: this.state.mail_error }}  >{this.state.mail_errorbody}</div>
                            <input className='stepinput' type="text" placeholder='نام کاربری' onChange={(e) => this.usernameok(e.target.value)} />
                            <div className='steperror' style={{ animation: this.state.username_error }} >{this.state.username_errorbody}</div>
                            <input className='stepinput' type="password" placeholder='رمز عبور' onChange={(e) => this.passwordok(e.target.value)} maxLength={16} />
                            <div className='steperror' style={{ animation: this.state.password_error }} >{this.state.password_errorbody}</div>
                        </div>
                        <div className='stepcontrol' >
                            <button className='stepforw' disabled={this.state.contEn} onClick={() => {
                                this.handleSubmite()
                            }} >
                                ادامه
                            </button>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobstepbox' style={{ animation: this.state.mobanimation }} >
                        <div className='mobstepdef' >
                            <p className='mobstepdefbig' >
                                اطلاعات خصوصی
                            </p>
                            <p className='stepdefsmall' >
                                ،{this.props.fullname} عزیز
                                <br />
                                پست الکترونیک، ایمیل و رمز عبور خود را تعیین کنید
                            </p>
                        </div>
                        <div className='mobstepbody' >
                            {this.state.errorMessage && <Message type="error">{this.state.errorMessage}</Message>}                            <input className='mobstepinput' type="text" placeholder='پست الکترونیک' onChange={(e) => this.mailok(e.target.value)} />
                            <div className='mobsteperror' style={{ animation: this.state.mobmail_error }}  >{this.state.mail_errorbody}</div>
                            <input className='mobstepinput' type="text" placeholder='نام کاربری' onChange={(e) => this.usernameok(e.target.value)} />
                            <div className='mobsteperror' style={{ animation: this.state.mobusername_error }} >{this.state.username_errorbody}</div>
                            <input className='mobstepinput' type="password" placeholder='رمز عبور' onChange={(e) => this.passwordok(e.target.value)} maxLength={16} />
                            <div className='mobsteperror' style={{ animation: this.state.mobpassword_error }} >{this.state.password_errorbody}</div>
                        </div>
                        <div className='mobstepcontrol' >
                            <button className='mobstepforw' disabled={this.state.contEn} onClick={() => {
                                this.handleSubmite()
                            }} >
                                ادامه
                            </button>
                        </div>
                    </div>
                </MediaQuery>
            </div>

        )
    }
}

export default Step2