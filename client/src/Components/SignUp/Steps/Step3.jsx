import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

// Style Region
import './Step3.css';
import './Step1.css';
// Style Region End

import camera from '../../../Assets/Images/signs/camera.png';

import AuthService from '../../../Containers/AuthService'
import { editeProfile } from '../../../Containers/AccountService'

class Step3 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bio: null,
            educs: null,
            work: null,
            phone: null,
            instagram: null,
            twitter: null,
            telegram: null
        }
        this.Auth = new AuthService()
        this.phoneref = React.createRef()

        this.username = this.props.username
        this.password = this.props.password
    }

    doLineInc() {

    }

    socialSet(key, value) {
        switch (key) {
            case 'phone':
                value = value.split('')
                var last = value[value.length - 1]
                if (!(Number(last) <= 9 && Number(last) >= 0)) {
                    value.pop()
                    this.phoneref.current.value = value.join('')
                }
                this.setState({
                    phone: value.join('')
                })
                break;
            case 'instagram':
                this.setState({
                    instagram: value
                })
                break;

            case 'twitter':
                this.setState({
                    twitter: value
                })
                break;

            case 'telegram':
                this.setState({
                    telegram: value
                })
                break;

            default:
                break;
        }
        console.log(this.state)
    }

    handleSubmite() {
        let imageFormObj = new FormData();

        if (this.state.userimage) {
            imageFormObj.append("avatar", this.state.userimage);
        }
        const data = { ...this.state }

        delete data.userimage
        for (let [key, value] of Object.entries(data)) {
            imageFormObj.append(key.toString(), value);
        }

        this.Auth.lgoin(this.username, this.password)
            .then(result => {
                if (result.error)
                    return /* this.handleError(result.error) */
                else if (!result.auth)
                    return /* this.handleError('رمز عبور اشتباه است') */
                else if (result.token) {
                    console.log(result);
                    
                    editeProfile(data, result.token)
                        .then(editeRresult => {
                            console.log(editeRresult);

                            if (editeRresult.success) {

                                this.Auth.setToken(result.token)
                                this.props.whenok();
                            } else {
                                alert('an error occured')
                            }
                        })
                }

                // this.handleError('something is wrong with server')
            })
            .catch(error => { this.handleError(error) })


            .catch(error => console.log(error))
    }

    render() {
        return (
            <div >
                <MediaQuery minWidth={766} >
                    <div className='step3box' >
                        <div className='stepdef' >
                            <p className='stepdefbig' >
                                تکمیل اطلاعات
                            </p>
                            <p className='stepdefsmall' >
                                اطلاعات تحصیل، کار و راه‌های ارتباطی خود را تکمیل و تنظیم کنید
                            </p>
                        </div>
                        <div className='step3body' >
                            <label for='userav' className='avatarspan' >
                                <img className='avimage' src={this.state.avatar || camera} alt="" />
                                <input id='userav' className='avatarinp' type="file" onChange={(e) => this.avatarJob(e.target.files)} />
                                <p className='avatarlabel' >
                                    تصویر پروفایل:
                                    </p>
                            </label>
                            <div className='inpgroup' >
                                <p className='inpdef' >
                                    بیو گرافی
                                    </p>
                                <textarea placeholder='بیوگرافی خود را وارد کنید' id='bio' className='stextarea' spellCheck={false} maxLength={140} ></textarea>
                            </div>
                            <div className='inpgroup' >
                                <p className='inpdef' >
                                    تحصیلات
                                    </p>
                                <textarea placeholder='اطلاعات مربوط به تحصیلات خود را وارد کنید' id='bio' className='stextarea' spellCheck={false} maxLength={100} ></textarea>
                            </div>
                            <div className='inpgroup' >
                                <p className='inpdef' >
                                    شغل
                                    </p>
                                <textarea placeholder='اطلاعات مربوط به شغل خود را وارد کنید' id='bio' className='stextarea' spellCheck={false} maxLength={100} ></textarea>
                            </div>
                            <div className='inpgroup' >
                                <p className='inpdef' >
                                    راه‌های ارتباطی
                                    </p>
                                <input className='stepinput' type="text" placeholder='شماره تماس' ref={this.phoneref} onChange={(e) => this.socialSet('phone', e.target.value)} maxLength={11} />
                                <input className='stepinput' type="text" placeholder='اینستاگرام' onChange={(e) => this.socialSet('instagram', e.target.value)} />
                                <input className='stepinput' type="text" placeholder='تویتر' onChange={(e) => this.socialSet('twitter', e.target.value)} />
                                <input className='stepinput' type="text" placeholder='تلگرام' onChange={(e) => this.socialSet('telegram', e.target.value)} />
                            </div>
                        </div>
                        <div className='stepcnptrol' >
                            <button className='stepforw' onClick={() => this.handleSubmite()} >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobstep3box' >
                        <div className='mobstepdef' >
                            <p className='mobstepdefbig' >
                                تکمیل اطلاعات
                            </p>
                            <p className='mobstepdefsmall' >
                                اطلاعات تحصیل، کار و راه‌های ارتباطی خود را تکمیل و تنظیم کنید
                            </p>
                        </div>
                        <div className='mobstep3body' >
                            <label for='mobuserav' className='mobavatarspan' >
                                <img className='mobavimage' src={this.state.avatar || camera} alt="" />
                                <input id='mobuserav' className='avatarinp' type="file" onChange={(e) => this.avatarJob(e.target.files)} />
                                <p className='mobavatarlabel' >
                                    تصویر پروفایل:
                                    </p>
                            </label>
                            <div className='mobinpgroup' >
                                <p className='mobinpdef' >
                                    بیو گرافی
                                    </p>
                                <textarea placeholder='بیوگرافی خود را وارد کنید' id='bio' className='mobstextarea' spellCheck={false} maxLength={140} ></textarea>
                            </div>
                            <div className='mobinpgroup' >
                                <p className='mobinpdef' >
                                    تحصیلات
                                    </p>
                                <textarea placeholder='اطلاعات مربوط به تحصیلات خود را وارد کنید' id='bio' className='mobstextarea' spellCheck={false} maxLength={100} ></textarea>
                            </div>
                            <div className='mobinpgroup' >
                                <p className='mobinpdef' >
                                    شغل
                                    </p>
                                <textarea placeholder='اطلاعات مربوط به شغل خود را وارد کنید' id='bio' className='mobstextarea' spellCheck={false} maxLength={100} ></textarea>
                            </div>
                            <div className='mobinpgroup' >
                                <p className='mobinpdef' >
                                    راه‌های ارتباطی
                                    </p>
                                <input className='mobstepinput' type="text" placeholder='شماره تماس' ref={this.phoneref} onChange={(e) => this.socialSet('phone', e.target.value)} maxLength={11} />
                                <input className='mobstepinput' type="text" placeholder='اینستاگرام' onChange={(e) => this.socialSet('instagram', e.target.value)} />
                                <input className='mobstepinput' type="text" placeholder='تویتر' onChange={(e) => this.socialSet('twitter', e.target.value)} />
                                <input className='mobstepinput' type="text" placeholder='تلگرام' onChange={(e) => this.socialSet('telegram', e.target.value)} />
                            </div>
                        </div>
                        <div className='mobstepcnptrol' >
                            <button className='mobstepforw' onClick={() => this.handleSubmite()} >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
            </div>

        )
    }
}

export default Step3