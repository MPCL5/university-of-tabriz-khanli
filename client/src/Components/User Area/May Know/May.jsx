import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'

// Style Region
import './May.css';
// Style Region End

import { getProfileImage } from '../../../Containers/Profile'

class May extends Component {
    constructor(props) {
        super(props)
        this.fullName = props.fullName
        this.bio = props.bio
        this.username = props.username
        this.image = getProfileImage(props.profileImage)

        // this.handleButtonClick = this.handleButtonClick.bind(this)
    }

    handleButtonClick(e) {
        // e.preventDefault()
        console.log(this.state);
    }


    render() {
        return (
            <>
                <MediaQuery minWidth={766} >
                    <div className="mayresult">
                        <img className='mayexactimage' src={this.image} alt="" />
                        <div className="maytextArea">
                            <h2 className='may-h2' >{this.fullName}</h2>
                            <p className='may-p'>{this.bio}</p>
                        </div>
                        <Link className='maybuttonArea' to={`../users/${this.username}`} target="_blanck">
                            <button className='maybutton' onClick={e => this.handleButtonClick(e)}>پروفایل</button>
                        </Link>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className="mobmayresult">
                        <img className='mobmayexactimage' src={this.image} alt="" />
                        <div className="mobmaytextArea">
                            <h2 className='mobmay-h2' >{this.fullName}</h2>
                            <p className='mobmay-p' >{this.bio}</p>
                        </div>
                        <Link className='mobmaybuttonArea' to={`../users/${this.username}`} target="_blanck">
                            <button className='mobmaybutton' onClick={e => this.handleButtonClick(e)}>پروفایل</button>
                        </Link>
                    </div>
                </MediaQuery>
            </>
        )
    }
}

export default May