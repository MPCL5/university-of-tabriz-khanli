import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Image Region
import phone from '../../../Assets/Images/phone.png';
import instagram from '../../../Assets/Images/instagram.png';
import twitter from '../../../Assets/Images/twitter.png';
import telegram from '../../../Assets/Images/telegram.png';
import camera from '../../../Assets/Images/signs/camera.png';
//Image Region End

//Style Region
import './UserEdit.css';
//Style Region End

//Component Region
import Menu from '../../Menu (and Head)/Menu';
import Message from '../../Message Area/Message'
//Component Region End

//Container Region
import profileRequest from '../../../Containers/Profile'
import AuthService from '../../../Containers/AuthService'
import WithAuth from '../../../Containers/WithAuth'
import { editeProfile, editeProfileImage } from '../../../Containers/AccountService'
//Container Region End

class UserEdit extends Component {
    constructor() {
        super()
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
        this.userNameRef = React.createRef()

        this.Auth = new AuthService()
    }

    state = {
        fullName: '',
        field: '',
        username: '',
        shortBio: '',
        bio: '',
        job: '',
        educations: '',
        phone: '',
        telegram: '',
        instagram: '',
        twitter: '',
        email: '',
        userimage: null,
        succcess: false,
        error: ''
    }

    componentWillMount() {
        profileRequest(this.Auth.getUserTokenData().username)
            .then(result => {
                if (result.error) return this.setState({ error: result.error })
                this.setState(pervState => ({ ...pervState, ...result }))
                console.log(this.state);

            })
            .catch(error => this.setState({ error: error }))
    }

    avatarJob(files) {
        var file = URL.createObjectURL(files[0])
        this.setState({
            userimage: file
        })
    }

    handleSubmite() {
        this.setState({ succcess: false, error: '' })

        let imageFormObj = new FormData();

        if (this.state.userimage) {
            imageFormObj.append("avatar", this.state.userimage);
        }
        const data = { ...this.state }

        delete data.userimage
        for (let [key, value] of Object.entries(data)) {
            imageFormObj.append(key.toString(), value);
        }

        const token = this.Auth.getToken();


        editeProfile(data, token)
            .then(result => {
                if (result.success) {
                    if (this.state.imageData) {
                        editeProfileImage(this.state.imageData, this.Auth.getToken())
                            .then(imageResult => {
                                // alert('image updaate successfull')
                                console.log(imageResult.data);

                                localStorage.setItem('profileImage', imageResult.data.fileName)
                                this.setState({ succcess: true })
                            })
                    } else {
                        this.setState({ succcess: true })
                    }

                } else {
                    this.setState({error: result.error})
                }
            })
            .catch(error => this.setState({error: error}))
    }

    handleCancel() {
        window.location.href = window.location.origin + '/uarea'
    }

    changeProfileImage(e) {
        if (!e.target.files[0])
            return

        this.setState({
            userimage: URL.createObjectURL(e.target.files[0])
        });

        const imageFormObj = new FormData();
        imageFormObj.append("avatar", e.target.files[0]);

        this.setState({ imageData: imageFormObj })
    }

    render() {
        return (
            <div className='edit-all' >
                <Menu backRef={this.backRef} mobBackRef={this.mobBackRef} />
                <MediaQuery minWidth={766}>
                    <div className='edit-father' ref={this.backRef} >
                        <div className='edit-def-father' >
                            <p className='edit-big-def' >
                                ویرایش اطلاعات
                            </p>
                            <p className='edit-small-def' >
                                می‌توانید اطلاعات خود را از اینجا ویرایش کنید
                            </p>
                            {this.state.succcess && <Message type="simple">اطلاعات شما با موفقیت ذخیره شد</Message>}
                            {this.state.error && <Message type="error">{this.state.error}</Message>}
                        </div>
                        <div className='edit-image-label' >
                            <label for='userav' className='avatarspan' style={{ width: '20%' }} >
                                <img className='avimage' src={this.state.userimage || camera} alt="" />
                                <input id='userav' className='avatarinp' type="file" onChange={e => { this.changeProfileImage(e) }} />
                                <p className='avatarlabel' >
                                    تصویر پروفایل:
                                        </p>
                            </label>
                        </div>
                        <div className='edit-area-father' >
                            {/* getting usee image */}

                            <div className='edit-main-father' >
                                <div className='main-child' >
                                    <p className='main-child-def' >
                                        نام کاربری
                                    </p>
                                    <input className='child-input' placeholder='@' style={{ border: this.state.usernamestyle, direction: 'ltr' }} type="text" maxLength={100} onChange={(e) => {
                                        this.setState({ username: e.target.value })
                                    }} value={'@' + this.state.username} disabled />
                                </div>
                                <div className='main-child' >
                                    <p className='main-child-def' >
                                        بیوگرافی کوتاه
                                    </p>
                                    <input className='child-input' value={this.state.shortBio} id='bio' tyape="text" maxLength={100} onChange={(e) => this.setState({ shortBio: e.target.value })} />
                                </div>
                                <div className='main-child' >
                                    <p className='main-child-def' >
                                        بیوگرافی
                                    </p>
                                    <textarea className='child-tarea' value={this.state.bio} name="" id='bio' cols="30" rows="10" maxLength={240} onChange={(e) => this.setState({ bio: e.target.value })}></textarea>
                                    {/* <input className = 'child-input' type="text" maxLength = {300} onChange = {(e) => this.setState({biofull: e.target.value})} /> */}
                                </div>
                                <div className='main-child' >
                                    <p className='main-child-def' >
                                        شغل
                                    </p>
                                    <input className='child-input' value={this.state.job} id='job' type="text" maxLength={200} onChange={(e) => this.setState({ job: e.target.value })} />
                                </div>
                                <div className='main-child' >
                                    <p className='main-child-def' >
                                        تحصیلات
                                    </p>
                                    <input className='child-input' value={this.state.educations} id='educations' type="text" maxLength={300} onChange={(e) => this.setState({ educations: e.target.value })} />
                                </div>
                            </div>
                            <div className='edit-extra-father' >
                                <div className='edit-extra-connect' >
                                    <div className='main-child' >
                                        <p className='main-child-def' >
                                            پست الکترونیک
                                        </p>
                                        <input className='child-input' value={this.state.email} id='email' type="text" maxLength={40} onChange={(e) => this.setState({ email: e.target.value })} />
                                    </div>
                                    <div className='main-child' >
                                        <img className='extra-connect-image' src={phone} alt="" />
                                        <input className='child-input' value={this.state.phone} id='phone' type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ phone: e.target.value })} />
                                    </div>
                                    <div className='main-child' >
                                        <img className='extra-connect-image' src={instagram} alt="" />
                                        <input className='child-input' value={this.state.instagram} id='instagram' type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ instagram: e.target.value })} />
                                    </div>
                                    <div className='main-child' >
                                        <img className='extra-connect-image' src={twitter} alt="" />
                                        <input className='child-input' value={this.state.twitter} id='twitter' type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ twitter: e.target.value })} />
                                    </div>
                                    <div className='main-child' >
                                        <img className='extra-connect-image' src={telegram} alt="" />
                                        <input className='child-input' value={this.state.telegram} id='telegram' type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ telegram: e.target.value })} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='edit-btn-group' >
                            <button className='edit-back-button' onClick={() => this.handleCancel()} >
                                برگشت
                            </button>
                            <button className='edit-submit-button' onClick={() => this.handleSubmite()} >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobedit-father' ref={this.mobBackRef} >
                        <div className='mobedit-def-father' >
                            <p className='mobedit-big-def' >
                                ویرایش اطلاعات
                            </p>
                            <p className='mobedit-small-def' >
                                می‌توانید اطلاعات خود را از اینجا ویرایش کنید
                            </p>
                        </div>
                        <div className='mobedit-area-father' >
                            {/* getting usee image */}
                            <div className='mobedit-main-father' >
                                <div className='mobmain-child' >
                                    <p className='mobmain-child-def' >
                                        نام کاربری
                                    </p>
                                    <input className='mobchild-input' value={'@' + this.state.username} placeholder='@' style={{ border: this.state.usernamestyle, direction: 'ltr' }} type="text" maxLength={100} onChange={(e) => {
                                        //check if username is valid
                                        var ok = true
                                        if (!ok) {
                                            this.setState({ username: e.target.value, usernamestyle: 'solid 1px red' })
                                        } else {
                                            this.setState({ username: '', usernamestyle: 'solid 1px green' })
                                        }
                                    }} disabled />
                                </div>
                                <div className='mobmain-child' >
                                    <p className='mobmain-child-def' >
                                        بیوگرافی کوتاه
                                    </p>
                                    <input className='mobchild-input' value={this.state.shortBio} type="text" maxLength={100} onChange={(e) => this.setState({ biosmall: e.target.value })} />
                                </div>
                                <div className='mobmain-child' >
                                    <p className='mobmain-child-def' >
                                        بیوگرافی
                                    </p>
                                    <textarea className='mobchild-tarea' value={this.state.bio} name="" id="" cols="30" rows="10" maxLength={240} ></textarea>
                                    {/* <input className = 'mobchild-input' type="text" maxLength = {300} onChange = {(e) => this.setState({biofull: e.target.value})} /> */}
                                </div>
                                <div className='mobmain-child' >
                                    <p className='mobmain-child-def' >
                                        شغل
                                        </p>
                                    <input className='mobchild-input' value={this.state.job} type="text" maxLength={200} onChange={(e) => this.setState({ job: e.target.value })} />
                                </div>
                                <div className='mobmain-child' >
                                    <p className='mobmain-child-def' >
                                        تحصیلات
                                        </p>
                                    <input className='mobchild-input' value={this.state.educations} type="text" maxLength={300} onChange={(e) => this.setState({ educs: e.target.value })} />
                                </div>
                            </div>
                            <div className='mobedit-extra-father' >
                                <div className='mobedit-extra-connect' >
                                    <div className='mobmain-child' >
                                        <img className='mobextra-connect-image' src={phone} alt="" />
                                        <input className='mobchild-input' value={this.state.phone} type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ phone: e.target.value })} />
                                    </div>
                                    <div className='mobmain-child' >
                                        <img className='mobextra-connect-image' src={instagram} alt="" />
                                        <input className='mobchild-input' value={this.state.telegram} type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ instagram: e.target.value })} />
                                    </div>
                                    <div className='mobmain-child' >
                                        <img className='mobextra-connect-image' src={twitter} alt="" />
                                        <input className='mobchild-input' value={this.state.twitter} type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ twitter: e.target.value })} />
                                    </div>
                                    <div className='mobmain-child' >
                                        <img className='mobextra-connect-image' src={telegram} alt="" />
                                        <input className='mobchild-input' value={this.state.telegram} type="text" maxLength={200} style={{ direction: 'ltr' }} onChange={(e) => this.setState({ telegram: e.target.value })} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='mobedit-btn-group' >
                            <button className='mobedit-back-button' onClick={() => this.handleCancel()}>
                                برگشت
                            </button>
                            <button className='mobedit-submit-button' onClick={() => this.handleSubmite()} >
                                ثبت اطلاعات
                            </button>
                        </div>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default WithAuth(UserEdit, true)