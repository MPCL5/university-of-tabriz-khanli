import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Image Region
import phone from '../../Assets/Images/phone.png';
import instagram from '../../Assets/Images/instagram.png';
import twitter from '../../Assets/Images/twitter.png';
import telegram from '../../Assets/Images/telegram.png';
//Image Region End

//Style Region
import './UserArea.css';
//Style Region End

//Component Region
import Menu from '../Menu (and Head)/Menu';
import May from './May Know/May';
//Component Region End

//Container Region
import profileRequest from '../../Containers/Profile';
import AuthService from '../../Containers/AuthService';
import { getSuggestions } from '../../Containers/AccountService'
import WithAuth from '../../Containers/WithAuth'
//Container Region End

class USerArea extends Component {
    constructor() {
        super()
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
        this.Auth = new AuthService()

    }

    state = {
        fullName: '',
        field: '',
        username: '',
        shortBio: '',
        bio: '',
        job: '',
        suggests: [],
        educations: ''
    }

    componentWillMount() {
        profileRequest(this.Auth.getUserTokenData().username)
            .then(result => {
                if (result.error) return this.setState({ error: result.error })
                this.setState(pervState => ({ ...pervState, ...result }))
                console.log(this.state);

            })
            .catch(error => this.setState({ error: error }))

        getSuggestions()
            .then(results => this.setState({ suggests: results }))
    }

    render() {
        console.log(this.state.suggests);
        
        return (
            <div style={{ minHeight: '500px' }}>
                <Menu backRef={this.backRef} mobBackRef={this.mobBackRef} />
                <MediaQuery minWidth={766} >
                    <div className='area-father' ref={this.backRef} >
                        <div className='area-me-father' >
                            {/* <img className = 'area-me-img' src={masoud} alt=""/> */}
                            <p className='area-me-name' >
                                {this.state.fullName}
                            </p>
                            <p className='area-me-field' >
                                {this.state.field}
                            </p>
                            <p className='area-me-username' >
                                @{this.state.username}
                            </p>
                            <p className='area-me-sbio' >
                                {this.state.shortBio}
                            </p>
                            <p className='area-me-fbio' >
                                {this.state.bio}
                            </p>
                            <div className='area-me-extra-father' >
                                <div className='area-me-extra-child' >
                                    <p className='extra-key' >
                                        شغل
                                </p>
                                    <p className='extra-value' >
                                        {this.state.job}
                                    </p>
                                </div>
                            </div>
                            <div className='area-me-extra-father' >
                                <div className='area-me-extra-child' >
                                    <p className='extra-key' >
                                        تحصیلات
                                </p>
                                    <p className='extra-value' >
                                        {this.state.educations}
                                    </p>
                                </div>
                            </div>
                            <div className='area-me-connect-father' >
                                <img className='connect-child' src={phone} alt="" />
                                <img className='connect-child' src={instagram} alt="" />
                                <img className='connect-child' src={twitter} alt="" />
                                <img className='connect-child' src={telegram} alt="" />
                            </div>
                            <button className='edit-btn' onClick={() => window.location.href = window.location.origin + '/uarea/edit'} >
                                ویرایش اطلاعات
                        </button>
                        </div>
                        <div className='area-maybe' >
                            <p className='area-maybe-title' >
                                افرادی که شاید بشناسید
                        </p>
                            <div className='area-maybe-other' >
                                { this.state.suggests.length ? this.state.suggests.map(item => (
                                    <May fullName={item.fullName} profileImage={item.profileImage} username={item.username} bio={item.bio} key={item.username} />
                                )) : <p>!هیچ شخصی یافت نشد</p>}
                            </div>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobarea-father' ref={this.mobBackRef} >
                        <div className='mobarea-me-father' >
                            {/* <img className = 'area-me-img' src={masoud} alt=""/> */}
                            <p className='mobarea-me-name' >
                                {this.state.fullName}
                            </p>
                            <p className='mobarea-me-field' >
                                {this.state.field}                        </p>
                            <p className='mobarea-me-username' >
                                @{this.state.username}
                            </p>
                            <p className='mobarea-me-sbio' >
                                {this.state.shortBio}
                            </p>
                            <p className='mobarea-me-fbio' >
                                {this.state.bio}
                            </p>
                            <div className='mobarea-me-extra-father' >
                                <div className='mobarea-me-extra-child' >
                                    <p className='mobextra-key' >
                                        شغل
                                </p>
                                    <p className='mobextra-value' >
                                        {this.state.job}
                                    </p>
                                </div>
                            </div>
                            <div className='mobarea-me-extra-father' >
                                <div className='mobarea-me-extra-child' >
                                    <p className='mobextra-key' >
                                        تحصیلات
                                </p>
                                    <p className='mobextra-value' >
                                    {this.state.educations}
                                </p>
                                </div>
                            </div>
                            <div className='mobarea-me-connect-father' >
                                <img className='mobconnect-child' src={phone} alt="" />
                                <img className='mobconnect-child' src={instagram} alt="" />
                                <img className='mobconnect-child' src={twitter} alt="" />
                                <img className='mobconnect-child' src={telegram} alt="" />
                            </div>
                            <button className='mobedit-btn' onClick={() => window.location.href = window.location.origin + '/uarea/edit'} >
                                ویرایش اطلاعات
                        </button>
                        </div>
                        <div className='mobarea-maybe' >
                            <p className='mobarea-maybe-title' >
                                افرادی که شاید بشناسید
                        </p>
                            <div className='mobarea-maybe-other' >
                                {this.state.suggests.map(item => (
                                    <May fullName={item.fullName} profileImage={item.profileImage} username={item.username} bio={item.bio} key={item.username} />
                                ))}

                            </div>
                        </div>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default WithAuth(USerArea, true)