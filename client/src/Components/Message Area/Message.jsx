import React, {Component} from 'react';

import './Message.css';

// import times from '../../Assets/Images/times.png';

class Message extends Component {
    constructor (props) {
        super(props)
        var style = ''
        switch (props.type) {
            case 'error':
                style = 'type-error';
            case 'warning':
                style = 'type-warning';
            case 'simple':
                style = 'type-simple';
            default:
                style = null;
        }
        this.state = {
            style: style,
            isShown: this.props.isShown || true,
            anim: 'showMessage 500ms 0ms forwards'
        }
    }

    
    componentWillMount () {
        var style = ''
        // console.log(this.props.type)
        switch (this.props.type) {
            case 'error':
                style = 'type-error';
                break;
            case 'warning':
                style = 'type-warning';
                break;
            case 'simple':
                style = 'type-simple';
                break;
            default:
                style = null;
                break;
        }
        this.setState ({
            style: style
        })        
    }

    render () {
        return (
            this.state.isShown ? (
                <div className = {'message-father ' + this.state.style} style = {{animation: this.state.anim}} >
                        <div className = 'message-body-father' >
                            <p className = 'message-text' >
                            {this.props.children}
                            </p>
                        </div>
                    <div className = 'message-remove-father' >
                        {/* <img className = 'message-remove' src = {times} alt="" onClick = {() => this.setState ({isShown: false})} /> */}
                    </div>
                </div>
            ) : (
                <></>
            )
        )
    }
}

export default Message