import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

//Image Region
import options from '../../Assets/Images/menuitems/options.png'
//Image Region

//Style Region
import './SignIn.css'
//Style Region End

//Component Region
import Menu from '../Menu (and Head)/Menu';
import Message from '../Message Area/Message'
//Component Region End

//Container Region
import AuthService from '../../Containers/AuthService'
import withAuth from '../../Containers/WithAuth'
//Container Region End

class SignIn extends Component {
    constructor(props) {
        super(props)
        this.Auth = new AuthService()
        this.state = {
            username: '',
            password: '',
            error: ''
        }
    }

    handleError = (error, clearPassword = true) => {
        this.setState({ error: error })
        if (clearPassword) {
            document.getElementById('password').value = ''
            this.setState({ password: '' })
        }

        console.log(error);
    }

    handleCahnge = e => {
        this.setState({ [e.target.id]: e.target.value })
    }

    handelSubmit = e => {
        e.preventDefault()
        this.state.error && this.handleError('')

        if (!this.checkFields()) {
            return this.handleError('لطفا جا های خالی را پر کنید');
        }

        const username = this.state.username
        const password = this.state.password

        this.Auth.lgoin(username, password)
            .then(result => {
                if (result.error) return this.handleError(result.error)
                else if (!result.auth) return this.handleError('رمز عبور اشتباه است')
                else if (result.token) {
                    // set the auth token
                    this.Auth.setToken(result.token)

                    // set the image url to local storage
                    localStorage.setItem('profileImage', result.profileImage)
                    
                    return this.props.history.replace('/uarea');
                }
                this.handleError('something is wrong with server')
            })
            .catch(error => { this.handleError(error) })
        // console.log(this.state);

    }

    // check the fields values.
    checkFields = () => {
        if (!this.state.username || !this.state.password)
            return false
        else
            return true
    }

    render() {
        return (
            <div>
                <Menu />
                <MediaQuery minWidth={766} >
                    <div className='containall' >
                        <div className='bigbox' >
                            <div className='boxhead' >
                                <p className='headbig'>
                                    ورود
                                </p>
                                <p className='headsmall' >
                                    برای ورود به فضای کاربری خود اطلاعات خود را وارد کنید
                                </p>
                            </div>
                            <form className='boxbody' onSubmit={this.handelSubmit}>
                                {this.state.error && <Message type="error">{this.state.error}</Message>}
                                <input className='nameormailinput' type="text" placeholder='ایمیل یا نام کاربری' id='username'
                                    onChange={this.handleCahnge} />
                                <input className='passwordinput' type="password" placeholder='رمز عبور' id='password'
                                    onChange={this.handleCahnge} />
                                <button className='signsubmit' >ورود</button>
                            </form>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={331} maxWidth={765} >
                    <div className='mobcontainall' >
                        <p className='mobheadbig'>
                            ورود
                        </p>
                        <p className='mobheadsmall' >
                            برای ورود به فضای کاربری خود اطلاعات خود را وارد کنید
                        </p>
                        <form className='mobboxbody' onSubmit={this.handelSubmit}>
                            {this.state.error && <Message type="error">{this.state.error}</Message>}
                            <input className='mobnameormailinput' type="text" placeholder='ایمیل یا نام کاربری' id='username'
                                onChange={this.handleCahnge} />
                            <input className='mobpasswordinput' type="password" placeholder='رمز عبور' id='password'
                                onChange={this.handleCahnge} />
                            <button className='mobsignsubmit' >ورود</button>
                        </form>
                    </div>
                </MediaQuery>
            </div>
        )
    }
}

export default withAuth(SignIn, false)