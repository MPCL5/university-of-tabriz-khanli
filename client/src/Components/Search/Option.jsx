import React, {Component} from 'react';
import MediaQuery from 'react-responsive';

// Style Region
import './Option.css'
// Style Region End

class Option extends Component {
    constructor (props) {
        super (props)
        this.state = {
            style: {

            },
            toggleBool: false
        }
    }

    toggle () {
        if (!this.state.toggleBool){
            this.setState({
                style:  {
                    backgroundColor: 'rgb(4, 139, 33)',
                    marginLeft: '0px'
                },
                toggleBool: true
            })
            this.props.toggleBack (this.props.id, true)
        }
        else{
            this.setState({
                style:  {
                    backgroundColor: 'rgb(117, 117, 117)',
                    marginLeft: '20px'
                },
                toggleBool: false
            })
            this.props.toggleBack (this.props.id, false)
        }
    }

    render () {
        return (
            <div className = 'option-toggle-father' >
                <p className = 'option-name' >
                    {this.props.name}
                </p>
                <div className = 'option-back' onClick = {this.toggle.bind(this)} >
                    <div className = 'option-circle' style = {this.state.style}  >
                    </div>
                </div>
            </div>
        )
    }
}

export default Option