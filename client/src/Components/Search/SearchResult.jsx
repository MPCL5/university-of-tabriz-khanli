import React, { Component } from 'react'
import { Link } from 'react-router-dom'

// Style Region
import './SearchResult.css'
// Style Region End

import image from '../../Assets/Images/masoud.jpg'

import { getProfileImage } from '../../Containers/Profile'

class SearchResult extends Component {
    constructor(props) {
        super(props)
        this.fullName = props.fullName
        this.bio = props.bio
        this.username = props.username
        this.image = getProfileImage(props.image)

        // this.handleButtonClick = this.handleButtonClick.bind(this)
    }

    handleButtonClick(e) {
        // e.preventDefault()

        console.log(this.state);

    }


    render() {
        return (
            <div className="result">
                <div className="imageArea">
                    <img src={this.props.profileImage} alt="" />
                </div>
                <div className="textArea">
                    <h2>{this.fullName}</h2>
                    <div><p>{this.bio}</p></div>
                </div>
                <div className="buttonArea">
                    <Link to={`../users/${this.username}`} target="_blanck">
                        <button onClick={e => this.handleButtonClick(e)}>پروفایل</button>
                    </Link>
                </div>
            </div>
        )
    }
}

export default SearchResult