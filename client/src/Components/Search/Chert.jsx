import React, { Component } from 'react'
import './Chert.css'


class Chert extends Component {
    state = {
        animation: null,
        showBody: true
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.hideBody === true) {
            return {
                ...prevState,
                animation: 'chert-anim',
                showBody: false
            }
        } else {
            return {
                ...prevState
            };
        }
    }

    showBody() {
        console.log(this.state);

        if (this.state.showBody) {
            return (
                <p>
                    شما در جستو جوی پیشرفته می توانید<br />
                    بر اساس هم ورودی ها رشته تحصیلی یا دانشکده خود<br />
                    دوستان خود را پیدا کرده و با آنها در ارتباط باشید
                </p>
            )
        } else {
            return null
        }
    }

    render() {
        console.log(this.state);

        return (
            <div className="chert-box">
                <div className="chert-head">جست و جوی پیشرفته</div>
                <div className={"chert-body " + this.state.animation}>
                    {this.showBody()}
                </div>
            </div>
        )
    }
}

export default Chert
