import React, { Component } from 'react';

// Style Region
import './Search.css';
// Style Region End

// Component Region
import Menu from '../Menu (and Head)/Menu';
import SearchResult from './SearchResult';
import Chert from './Chert';
// Component Region End

// Containers Region
import searchRequest from '../../Containers/Search'
// Containers Region End


class Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            query: '',
            results: [],
            isFirstTime: true,
            hideBody: false,
            isQuering: false
        }
        this.backRef = React.createRef()
        this.mobBackRef = React.createRef()
        this.handleQueryChange = this.handleQueryChange.bind(this)
        this.showSearchResults = this.showSearchResults.bind(this)
    }

    componentDidMount() {
        console.log(this.state);

    }

    handleQueryChange(e) {
        this.setState({ query: e.target.value })

        // check the length of query
        if (e.target.value.length > 3) {
            // show the quering text
            this.setState(pervState => ({ ...pervState, isQuering: true }))

            searchRequest(e.target.value)
                .then(results => {
                    this.setState(prevState => {
                        // hide the quering text
                        prevState = { ...prevState, isQuering: false };

                        if (this.state.isFirstTime)
                            return {
                                ...prevState,
                                isFirstTime: false,
                                hideBody: true,
                                results: results
                            }
                        else
                            return {
                                ...prevState,
                                results: results
                            }
                    })
                })
                .catch(error => {
                    this.setState({ error: error })
                })
        } else {
            this.setState({ results: [] })
        }
    }

    // handel the search result area style.
    showSearchResults() {
        console.log(this.state.results.length);

        return (
            <div className='searhbox-result-father' >
                <p>{this.state.results.length !== 0 ? ":نتایج جستجو" : this.state.isQuering ? "...در حال جستجو" : "!هیچ موردی یافت نشد"}</p>
                {this.state.results.map(item => ( 
                    <SearchResult fullName={item.fullName} profileImage={item.profileImage} username={item.username} bio={item.bio} key={item.username} />
                ))}
            </div>
        )
    }

    render() {
        return (
            <>
                <Menu backRef = {this.backRef} mobBackRef = {this.mobBackRef} />                
                <div className='searchbox-all' ref = {this.backRef}>
                    <Chert hideBody={this.state.hideBody} />
                    <div className='searchbox-inp-father' >
                        <button className='searchbox-button' >
                            جست و جو
                    </button>
                        <input type="text" value={this.state.query} className='searchbox-input' onChange={this.handleQueryChange} />
                    </div>
                    <div className='searhbox-options-father' ref={this.mobBackRef}>
                        :مرتب سازی بر اساس
                    <select name="sortBy" id="">
                            <option value="fatually">دانشکده</option>
                            <option value="yearOfJoin">سال ورود</option>
                            <option value="field">رشته تحصیلی</option>
                        </select>
                    </div>
                    {!this.state.isFirstTime && this.showSearchResults()}
                </div>
            </>
        )
    }
}

export default Search