import React, { Component } from 'react'

import "./Member.css"

import { getProfileImage } from '../../Containers/Profile'


class Member extends Component {
    constructor(props) {
        super(props)

        this.profileImageSource = getProfileImage(props.profileImage)
        this.name = props.name
        this.field = props.field
        this.yearOfJoin = props.yearOfJoin || '97'
    }

    render() {
        return (
            <div className="friends-member">
                <div className="friends-member-image">
                    <img src={this.profileImageSource} alt="" />
                </div>
                <div className="friends-member-info">
                    <div className="friends-member-info-name">{this.name}</div>
                    <div className="friends-member-info-field">{`${this.yearOfJoin} ${this.field}`}</div>
                </div>
                <div className="friends-member-button">
                    <button>پروفایل</button>
                </div>
            </div>
        )
    }
}

export default Member